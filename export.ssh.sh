#!/bin/sh

dest_server="pegasus.generation-i.com"

case `hostname` in
	'caladan' | 'caladan.generation-i.com')
		host_id=55;;
	'regulas2' | 'regulas2.generation-i.com')
		host_id=56;;
	'apollo' | 'apollo.generation-i.com')
		host_id=57;;
	'regulas3' | 'regulas3.generation-i.com')
		host_id=58;;
	'jupiter' | 'jupiter.generation-i.com')
		host_id=59;;
	*)
		host_id=99;;
esac


ports=" -R "$host_id"22:localhost:22 "$dest_server" -R "$host_id"80:localhost:80 "$dest_server
ssh_cmd="ssh -gnfNT "


if [ `whoami` != 'eric' ];
then
	echo $ssh_cmd $ports
	su eric -c "$ssh_cmd $ports"
else
	echo $ssh_cmd $ports
	$ssh_cmd $ports
fi

