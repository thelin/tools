#!/usr/bin/perl -w

while(<>)
{
	if(/^([0-9]+)\s/)
	{
		$num=$1;
		if($num>1024*1024*1024)
		{	
			$new_num=sprintf("%.1fG",($num/(1024*1024*1024)));
			$new_num=~s/([0-9]{2,})\.[0-9]([A-Z])$/$1$2/;
			s/^$num/$new_num/;
		}
		elsif($num>1024*1024)
		{
			$new_num=sprintf("%.1fM",($num/(1024*1024)));
			$new_num=~s/([0-9]{2,})\.[0-9]([A-Z])$/$1$2/;
			s/^$num/$new_num/;
		}
		elsif($num>1024)
		{
			$new_num=sprintf("%.1fK",($num/(1024)));
			$new_num=~s/([0-9]{2,})\.[0-9]([A-Z])$/$1$2/;
			s/^$num/$new_num/;
		}
	}
	print $_;
}
