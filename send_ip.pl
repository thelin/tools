#!/usr/bin/perl

# use strict;
use warnings;
use Data::Dumper;
# use Net::DNS;

my $mydyndns_server="http://www.generation-i.com/mydyndns.php";
if (! -f "/etc/mydyndns" || ! -r "/etc/mydyndns")
{
	die "$0: Error reading /etc/mydyndns\n";
}
my $cfg_mode=sprintf("%04o",(stat("/etc/mydyndns"))[2] & 07777);
if($cfg_mode ne '0600')
{
	die "$0: Permissions error on /etc/mydyndns (should be 0600)\n";
}

open(CFG,"/etc/mydyndns") || die "$0: unable to open /etc/mydyndns\n";

my %cfg;
while(my $line=<CFG>)
{
	if($line=~/^([a-z].*?)=['\"]?(.*?)[\"']?$/i)
	{
		$cfg{lc($1)}=$2;
	}
}

if(!exists($cfg{'host'}))
{
	$cfg{'host'}=`hostname`;
	chomp($cfg{'host'});
	$cfg{'host'}=$cfg{'host'};
}

# my $res = Net::DNS::Resolver->new(
	# nameservers => $cfg{name_server});
# my $answer = $res->search($cfg{'host'});
# use Data::Dumper; print Dumper($answer); # EKT
my $current_ip=`host $cfg{'host'} $cfg{name_server}|cut -d' ' -f4|tail -n 1`;
my $ip;
if(exists($cfg{ip}))
{
	if($cfg{ip}=~/([a-z]{3}[0-9])/)
	{
		my $ifconfig=`ifconfig $cfg{ip}|grep 'inet addr:'`;
		if($ifconfig=~/inet addr:([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})/)
		{
			$ip=$1;
		}
	}
	elsif($cfg{ip}=~/([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})/)
	{
		$ip=$cfg{ip};
	}
}
else
{
	$ip=`curl -s 'http://eric.thelin.org/get.my.ip.html'`;
}

if ($current_ip ne $ip)
{
	print `curl -s "$mydyndns_server?host=$cfg{host}&auth=$cfg{auth}&ip=$ip"`;
}

exit;
