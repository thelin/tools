#!/usr/bin/perl

use strict;

use Getopt::Long;
use Digest::MD5::File qw(file_md5_hex);
use File::stat;
use Image::ExifTool qw(:Public);
use File::Copy;


use Data::Dumper;
# my $option = process_options();

sub usage {
    print "
    Usage: $0 [directory]
    --help, -h                  This help message
    Optionals:
    --source=<path>, -s <path>  Source path
    --dest=<path>, -d <path>    Destination path
    --copy, -c                  Copy instead of move
    --force, -f                 Force move or copy
    --verbose, -v               Verbose output
    --dry_run, -n               Dry run
";
}

my %options;
Getopt::Long::Configure ("gnu_getopt");
GetOptions(
	'help|h'      => \$options{help},
	'source|s=s'  => \$options{source},
	'dest|d=s'    => \$options{destination},
	'copy|c'      => \$options{copy},
	'force|f'     => \$options{force},
	'dry_run|n'   => \$options{dry_run},
	'verbose|v+'  => \$options{verbose},
) || help();

help() if $options{help};


my $dest;

if ($options{destination}) {
    $dest=$options{destination};
} else {
    $dest="/home/eric/camera";
}
if (! -d $dest) {
    mkdir $dest,0755;
}

if ($options{source}) {
    push(@ARGV, $options{source});
}

if (scalar(@ARGV)==0) {
    my $pwd=`pwd`;
    chomp($pwd);

    if ($pwd=~/dcim\/1[0-9]+[a-z]+/i) {
        push(@ARGV, $pwd);
        print "adding $pwd from pwd\n" if $options{verbose};
    }  else {
        if (-d 'dcim') {
            chdir('dcim');
        } elsif (-d 'dcim') {
            chdir('DCIM');
        }
        opendir(DIR,'.');
        while(my $file=readdir(DIR)) {
            if ($file=~/1[0-9]+[a-z]+/i) {
                push(@ARGV, $file);
                print "adding $file from pwd/dcim\n" if $options{verbose};
            }
        }
        closedir(DIR);

        if (scalar(@ARGV) == 0)
        {
            my %dir_cache;
            while(my $camera_dirs=</media/*/[Dd][Cc][Ii][Mm]/1[0-9][0-9][A-Za-z][A-Za-z]*/*.[Jj][Pp][Gg]>) {
                $camera_dirs=~s#/[^/]*$##;
                if (not exists($dir_cache{$camera_dirs})) {
                    $dir_cache{$camera_dirs} = 1;
                    push(@ARGV, $camera_dirs);
                    print "adding $camera_dirs from global list\n" if $options{verbose};
                }

            }
        }

        if (scalar(@ARGV) == 0) {
            print "unknown path ($pwd)\n";
        }
    }
}

foreach my $source (@ARGV) {
    print "source=$source\n" if $options{verbose};
    print "dest=$dest\n" if $options{verbose};

    $source=~s#/$##g;
    opendir(DIR,$source);
    while(my $file=readdir(DIR)) {
        if ($file!~/\.(jpg|jpeg|png|avi|mpg|m4v|mp4|mpeg|mov)$/i) {
            next;
        }

        my @localtime=localtime((stat $source.'/'.$file)->mtime);
        my $time=sprintf("%04d%02d%02d",$localtime[5]+1900,$localtime[4]+1,$localtime[3]);
        my $year=sprintf("%04d",$localtime[5]+1900);

        if ($file=~/_(((19|20)\d\d)[01]\d[0123]\d)_/) {
            $time = $1;
            $year = $2;
        } elsif ($file=~/((((19|20)\d\d))-([01]\d)-([0123]\d))/) {
            $time = $2.$5.$6;
            $year = $2;
        } elsif ($file=~/\.(jpg|jpeg|png)/) {
            my @tags = ('CreateDate', 'ModifyDate', 'DateTimeOriginal');
            my $info = ImageInfo($source.'/'.$file);
            for my $tag (@tags) {
                if ($info->{$tag}=~/(\d{4})[:-](\d{2})[:-](\d{2})/) {
                    $time=$1.$2.$3;
                    $year=$1;
                    last;
                }
            }
        }

        if (!-d "$dest/$year/$time") {
            print"creating $dest/$time\n" if $options{verbose};
            if (!$options{dry_run}) {
                mkdir "$dest/$year", 0755;
                mkdir "$dest/$year/$time", 0755;
                print "\n" if $options{verbose};
                print `ls -l $dest/$year/$time` if $options{verbose};
            }
        }

        my $opts='';
        if ($options{force}) {
            $opts.=' -f';
        } else {
            $opts.=' -i';
        }
        my $out;
        my $dest_file=$file;
        $dest_file=~s/(...)$/\L$1/;
        if (-f "$dest/$year/$time/$dest_file") {
            my $dupCheck = checkDupes("$dest/$year/$time/$dest_file", "$source/$file");
            if ($dupCheck eq "same") {
                print "same $source/$file $dest/$year/$time/$dest_file\n";
                next;
            }
            if ($dupCheck eq "dup") {
                print "dup $source/$file $dest/$year/$time/$dest_file\n";
                if (!$options{copy} && !$options{dry_run}) {
                    unlink("$source/$file");
                    # `rm $source/$file`;
                }
                next;
            }
        }

        if ($options{copy}) {
            print "copy $source/$file $dest/$year/$time/$dest_file\n" if $options{verbose} || $options{dry_run};
            copy("$source/$file", "$dest/$year/$time/$dest_file") if !$options{dry_run};
        } else {
            print "move $source/$file $dest/$year/$time/$dest_file\n" if $options{verbose} || $options{dry_run};
            move("$source/$file", "$dest/$year/$time/$dest_file") if !$options{dry_run};
        }
        print "$dest/$year/$time $file\n" if !$options{dry_run};
    }
}

sub checkDupes {
    my ($file1, $file2) = @_;
    my $stat1 = stat($file1);
    my $stat2 = stat($file2);
    if ($stat1->ino eq $stat2->ino) {
        return "same";
    }
    if (md5($file1) eq md5($file2)) {
        return "dup";
    }

    print "NOT dup [[$file1 == $file2]]\n" if $options{verbose} > 1;
    return "unique";
}

sub md5 {
    my ($file) = @_;
    # print file_md5_hex($_[0]),"\n";
    return file_md5_hex($_[0]);

    # $file=~s/ /\\ /g;
    # my $md5 = `md5sum "$file"|cut -d' ' -f1`;
    # chomp($md5);
    # return $md5;
}
