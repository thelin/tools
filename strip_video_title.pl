#!/usr/bin/perl

# sudo apt install mkvtoolnix
# sudo apt install ffmpeg

use warnings;
use strict;

for my $arg (@ARGV) {
    my $origSize = -s $arg;

    my ($baseName, $extension) = $arg =~ /^(.*)\.(.*?)$/;
    if (-d $arg || !defined($extension) ||$extension =~ /jpg|srt/) {
        next; # Skip known unrelated files
    } elsif ($extension eq "mkv") {
        my $info = `mkvinfo "$arg"`;
        if ($info !~ /^\| \+ Title:/m) {
            # print $info;
            print "Skipping $arg (no title found)\n";
            next;
        }
    } elsif ($extension =~ /mp4|m4v|avi/) {
        my $info = `exiftool "$arg"`;
        if ($info !~ /^Title\s*: /m) {
            print "Skipping $arg (no title found)\n";
            next;
        }
    } else {
        print "Unknown file type $extension : $arg\n";
        next;
    }
    # print "~~~~~~~~~~ $arg\n";
    # next;

    (my $tmpFile = $arg) =~ s/\.(.*?)$/.tmp.$1/;
    my $out = `ffmpeg -i "$arg" -c copy -metadata title= "$tmpFile"`;
    my $res = $?;
    my $newSize = -s $arg;
    my ($readTime, $writeTime) = (stat($arg))[8,9];

    utime($readTime, $writeTime, $tmpFile);

    print "$res ~ $arg -- $origSize ($newSize) $tmpFile\n";
    if ($res == 0 && $newSize > 0 && $newSize > ($origSize * 0.85)) {
        rename($tmpFile, $arg);
        print ":)\n";
    }
}
