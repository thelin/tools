#!/usr/bin/perl

# add the extension of a file based on its file type
# this is frequently useful for browser cache files which don't have valid names

foreach $arg (@ARGV)
{
	$type=`file $arg`;
	if($type=~/JPEG/)
	{
		`mv $arg $arg.jpg`;
	}
	elsif($type=~/GIF/)
	{
		`mv $arg $arg.gif`;
	}
	elsif($type=~/PNG/)
	{
		`mv $arg $arg.png`;
	}
	elsif($type=~/HTML/i)
	{
		`mv $arg $arg.html`;
	}
	elsif($type=~/text/i)
	{
		`mv $arg $arg.txt`;
	}
	elsif($type=~/gzip/i)
	{
		`mv $arg $arg.gz`;
	}
	elsif($type=~/bzip2/i)
	{
		`mv $arg $arg.bz2`;
	}
	elsif($type=~/MIDI/i)
	{
		`mv $arg $arg.mid`;
	}
	elsif($type=~/MPEG/i)
	{
		`mv $arg $arg.mpg`;
	}
	else
	{
		print "unknown type $type\n";
	}
}
