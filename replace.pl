#!/usr/bin/perl
#
# This is a replacement for the

use warnings;
use strict;
use Data::Dumper;
use Getopt::Long;

# use File::Inplace;

use constant PAIRS => 1;
use constant FILES => 2;

my $help;
my $backup;
my $noCase;
my $verbose;


my @pairs;
my @files;
my $mode=PAIRS;
for(my $i=0; $i< scalar(@ARGV); $i++) {
    my $current = $ARGV[$i];
    if ($current eq '--') {
        if ($i%2 != 0 || $i == 0) {
            warn "1";
            dieMismatch();
        }
        $mode=FILES;
        next;
    } elsif ($current =~ /^\-/) {
        next;
    }
    if ($mode == PAIRS) {
        if (($i+1) >= scalar(@ARGV)) {
            warn "2";
            dieMismatch();
        }
        push(@pairs, [$current, $ARGV[++$i]]);
    } else {
        push(@files, $current);
    }
}

# warn Dumper(@ARGV);
# Getopt::Long::Configure ("gnu_getopt");
# GetOptions(
	# 'help|h'     => \$help,
	# 'backup|b=s' => \$backup,
	# 'insensitive|i'   => \$noCase,
	# 'verbose|v+' => \$verbose,
# ) || help();
#
# help() if $help;
#
# warn Dumper(@ARGV);

# my $bakSuffix = ".bak";
our $^I = 'bak';

if (scalar(@files) == 0) {
    push(@files, '-');
}

# warn Dumper(@pairs);
# warn Dumper(@files);


for my $file (@files) {
    if ($file eq '-') {
        while (<STDIN>) {
            print replace($_, \@pairs);
        }
    } else {
        # my $editor = new File::Inplace(file => $file, suffix => $bakSuffix);
        # while (my ($line) = $editor->next_line) {
            # $editor->replace_line(replace($line, \@pairs));
        # }
        # $editor->commit;

        our @ARGV = ($file);

        while ( <ARGV> ) {
            $_ = replace($_, \@pairs);
            # tr/a-z/A-Z/;
            print;
        }
        print "converted $file\n";
    }
}

sub dieMismatch {
    die "$0: No to-string for last from-string\n";
}

sub replace {
    my $line = $_[0];
    my $pairs = $_[1];

    for my $pair (@{$pairs}) {
        my ($from, $to) = @{$pair};
        if ($from=~/^\|(.*)\|$/) {
            $from=$1;
        } else {
            $from = quotemeta $from;
        }
        $line=~s/$from/$to/g;
    }
    return $line;
}

# sub help
# {
	# warn <<"END"
# Usage: $0 [options] from to [from to ...] [-- <files>]
  # --help, -h                 This message
  # --backup <ext>, -b <ext>   Extension to use for a backup file. No extension means no backup
  # --insensitive, -i           Replaces should be case insensitive
  # --verbose, -v              Verbose.  Show more output (can be repeated for
                             # even more output)
# END
# ;
	# exit 1;
# }
#
