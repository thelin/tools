#!/bin/bash

output_dir="/tmp/cdroms"

if [ -z $1 ]
then
	dr=/dev/cdroms/cdrom0
elif [ -e $1 ]
then
  dr=$1
elif [ -e "/dev/cdroms/$1" ]
then
	dr=/dev/cdroms/$1
elif [ -e "/dev/cdroms/cdrom$1" ]
then
	dr=/dev/cdroms/cdrom$1
else
  echo "unknown drive"
	exit 1
fi

if [ $dr == '/dev/cdroms/cdrom0' ]
then
	mount_dir="/mnt/cdrom"
elif [ $dr == '/dev/cdroms/cdrom1' ]
then
	mount_dir="/mnt/cdrw"
else
	echo "unknown drive for mounting ($dr)"
	exit 2
fi

label=`cdlabel.pl $dr`

echo $label

#mount $dr $mount_dir
mount $mount_dir

cd $mount_dir
find -ls >/tmp/cdroms/"$label"
cd ..
umount $mount_dir


cat "$output_dir"/"$label"
eject $dr
echo $label
