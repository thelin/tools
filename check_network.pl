#!/usr/bin/perl

use warnings;
use strict;

use Getopt::Std;

use Data::Dumper;
my $option = process_options();


do
{
	my %prg_output;
	$prg_output{'iwconfig'} = `iwconfig 2>&1`;
	$prg_output{'ifconfig'} = `ifconfig -a 2>&1`;
	
	my %interface;
	for my $prg (keys(%prg_output)) 
	{
		# print "prg=$prg\n";
		my @block = split /\n\n/,$prg_output{$prg};
		for my $line (@block)
		{
			if($line=~/^([a-z0-9]+)\s+(.*)/s)
			{
				my $int=$1;
				my $info=$2;
				$info=~s/^\s+//mg;
				$interface{$int}{$prg}=$info;
				# print "line=$int {$info}\n";
			}
			else
			{
				print "line=$line\n";
			}
		}
	}
	
	for my $int (keys(%interface))
	{
		# print $interface{$int}{'ifconfig'};
		if($interface{$int}{'ifconfig'}=~/inet addr:([0-9.]+)/)
		{
			$interface{$int}{'ip'}=$1;
		}
		if($interface{$int}{'iwconfig'}=~/ESSID:"(.+?)"/)
		{
			$interface{$int}{'SSID'}=$1;
		}
		if($interface{$int}{'iwconfig'}=~/IEEE (802.11[a-z])/)
		{
			$interface{$int}{'wireless'}=$1;
		}
	}
	
	if($option->{loop})
	{
		print `clear;date`."\n";
	}
	
	my $useable_ip;
	
	my %interface_raw=%interface;
	for my $int (keys(%interface))
	{
		delete($interface{$int}{'ifconfig'});
		delete($interface{$int}{'iwconfig'});
		if(scalar(keys(%{$interface{$int}}))>0)
		{
			print "$int\n";
			for my $key (keys(%{$interface{$int}}))
			{
				print "\t$key=".$interface{$int}{$key}."\n";
				if($key eq 'ip' && $int ne 'lo')
				{
					$useable_ip=$interface{$int}{$key};
				}
			}
			print "\n";
		}
	}
	if($option->{loop})
	{
		if($useable_ip)
		{
			sleep 5;
		}
		else
		{
			sleep 2;
		}
	}
	
} while($option->{loop});
# use Data::Dumper; warn Dumper(\%interface); # EKT

sub process_options {
	my %options = ();
	
	getopts('hl', \%options);
	
	if ($options{h}) { usage(); exit(); }
	
	# strip whitespace
	foreach (keys %options) {
		$options{$_} =~ s/^\s*//g;
		$options{$_} =~ s/\s*$//g;
	}
	
	my %map = (
		l => 'loop',
	);
	# map names
	foreach (keys %map) {
		$options{$map{$_}} = $options{$_} if exists $options{$_};
	}
	
	return \%options;
}


sub usage {
	print "
	Usage: $0 -h
	-h                  This help message
	Optionals:
	-l                  Loop
";

} 
