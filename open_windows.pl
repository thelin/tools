#!/usr/bin/perl -w

use Data::Dumper;
use Storable qw(dclone);
use strict;

my $TERM_BASE;
my %TERM;

my $host;
if(exists($ENV{HOSTNAME}))
{
	$host=$ENV{HOSTNAME};
}
elsif(-f '/etc/HOSTNAME')
{
	$host=`cat /etc/HOSTNAME`;
	chomp($host);
}
elsif(-f '/proc/sys/kernel/hostname')
{
	$host=`cat /proc/sys/kernel/hostname`;
	chomp($host);
}
# print "host=$host\n";

chdir($ENV{HOME});

$TERM{rxvt}='/usr/bin/rxvt -geometry 80x50 -ls -sl 500 +vb -cr blue -bg white -T [title] -fg black -e [command]';
$TERM{konsole}="/usr/bin/konsole --vt_sz 80x50 -T [title] -e [command]";
$TERM{'gnome-terminal'}='/usr/bin/gnome-terminal --tab  -t [title] --geometry 80x40 -e "[command]"'; #
$TERM{mrxvt}="/usr/local/bin/mrxvt -geometry 80x50 -ls -sl 1000 +vb -cr blue -bg white -T [title] -e [command]";
$TERM{aterm}="/usr/bin/aterm -geometry 80x50 -ls -sl 1000 +vb -cr blue -bg white -T [title] -e [command]";
$TERM{xterm}="/usr/bin/xterm -fn 7x14 -geometry 80x50 -ls -sl 500 +vb -cr blue -bg white -fg black -T [title] -e [command]";

my %servers;
$servers{DEFAULT}={
	f1=> { title=>'Bacon',
		command=>'ssh bacon' },
	# f1=> { title=>'Cygnus',
		# command=>'ssh -l eric cygnus.generation-i.com' },
	f2=> { title=>'Phact',
		command=>'ssh -l eric phact.generation-i.com' },
	f3=> { title=>'Ceres',
		command=>'ssh -l eric ceres.generation-i.com' },
	# f3=> { title=>'Pollux',
		# command=>'ssh -tXl eric phact.generation-i.com ssh -tX pollux-local' },
		# command=>'ssh -l eric pollux.generation-i.com' },
	f4=> { title=>'pluto',
		command=>'ssh pluto' },
	f12=> { title=>'localhost',
		command=>'' },
	TERM_BASE=>$TERM{xterm}
};
$servers{'etdev.ilpoints.com'}={
	f1=> { title=>'Cygnus',
		command=>'ssh -tXl eric jupiter.generation-i.com ssh -tX cygnus' },
	f5=> { title=>'cvs/svn',
		command=>'ssh cvs.incentivelogic.com' },
	f6=> { title=>'Zod',
		command=>'ssh zod' },
	f7=> { title=>'ursa',
		command=>'ssh ursa' },
	f8=> { title=>'Pluto2',
		command=>'ssh pluto2' },
	f9=> { title=>'proxima',
		command=>'ssh -tXl eric jupiter.generation-i.com ssh -tX proxima' },
		# command=>'ssh -tXl eric phact.generation-i.com ssh -tXl eric jupiter.generation-i.com ssh -tX proxima' },
	f10=> { title=>'Pan',
		command=>'ssh -tXl eric jupiter.generation-i.com ssh -tX pan' },
		# command=>'ssh -tXl eric phact.generation-i.com ssh -tXl eric jupiter.generation-i.com ssh -tX apollo' },
	f11=> { title=>'Jupiter',
		command=>'ssh -Xl eric jupiter.generation-i.com' },
		# command=>'ssh -tXl eric phact.generation-i.com ssh -Xl eric jupiter.generation-i.com' },
	f12=> { title=>'etdev',
		command=>'' },
	TERM_BASE=>$TERM{rxvt}
};
$servers{'etdev-gnome'}={
	f1=> { title=>'Cygnus',
		command=>'cygnus' },
	f5=> { title=>'cvs/svn',
		command=>'drcs001' },
	f6=> { title=>'Zod',
		command=>'zod' },
	f7=> { title=>'ursa',
		command=>'ursa' },
	f8=> { title=>'Pluto',
		command=>'pluto' },
	f9=> { title=>'Proxima',
		command=>'proxima' },
		# command=>'ssh -tXl eric phact.generation-i.com ssh -tXl eric jupiter.generation-i.com ssh -tX proxima' },
	f10=> { title=>'Pan',
		command=>'pan' },
		# command=>'ssh -tXl eric phact.generation-i.com ssh -tXl eric jupiter.generation-i.com ssh -tX pan' },
	f11=> { title=>'Jupiter',
		command=>'jupiter' },
		# command=>'ssh -tXl eric phact.generation-i.com ssh -Xl eric jupiter.generation-i.com' },
	f12=> { title=>'etdev',
		command=>'' },
	TERM_BASE=>'/usr/bin/gnome-terminal --geometry 80x40 --tab-with-profile=[command]',
};
$servers{'ethelin-desktop'}={
	f1=> { title=>'Cygnus',
		command=>'ssh -tXl eric jupiter.generation-i.com ssh -tX cygnus' },
	f5=> { title=>'cvs/svn',
		command=>'ssh dev3.int' },
	f6=> { title=>'Zod',
		command=>'ssh zod' },
	f7=> { title=>'ursa',
		command=>'ssh ursa' },
	f8=> { title=>'Pluto2',
		command=>'ssh pluto2' },
	f9=> { title=>'Proxima',
		command=>'ssh -tXl eric jupiter.generation-i.com ssh -tX proxima' },
		# command=>'ssh -tXl eric phact.generation-i.com ssh -tXl eric jupiter.generation-i.com ssh -tX proxima' },
	f10=> { title=>'Pan',
		command=>'ssh -tXl eric jupiter.generation-i.com ssh -tX pan' },
		# command=>'ssh -tXl eric phact.generation-i.com ssh -tXl eric jupiter.generation-i.com ssh -tX apollo' },
	f11=> { title=>'Jupiter',
		command=>'ssh -Xl eric jupiter.generation-i.com' },
		# command=>'ssh -tXl eric phact.generation-i.com ssh -Xl eric jupiter.generation-i.com' },
	f12=> { title=>'etdev',
		command=>'' },
	TERM_BASE=>$TERM{'gnome-terminal'}
};
$servers{'ethelin-desktop-gnome'}={
	f1=> { title=>'Cygnus',
		command=>'cygnus' },
	f5=> { title=>'dev3',
		command=>'dev3' },
	# f6=> { title=>'Zod',
		# command=>'zod' },
	# f7=> { title=>'ursa',
		# command=>'ursa' },
	f8=> { title=>'Pluto',
		command=>'pluto' },
	f9=> { title=>'Proxima',
		command=>'proxima' },
		# command=>'ssh -tXl eric phact.generation-i.com ssh -tXl eric jupiter.generation-i.com ssh -tX proxima' },
	f10=> { title=>'Pan',
		command=>'pan' },
		# command=>'ssh -tXl eric phact.generation-i.com ssh -tXl eric jupiter.generation-i.com ssh -tX pan' },
	f11=> { title=>'Jupiter',
		command=>'jupiter' },
		# command=>'ssh -tXl eric phact.generation-i.com ssh -Xl eric jupiter.generation-i.com' },
	f12=> { title=>'etdev',
		command=>'' },
	TERM_BASE=>'/usr/bin/gnome-terminal --geometry 80x40 --tab-with-profile=[command]',
};
$servers{'saturn.generation-i.com'}={
	f3=> { title=>'etdev1',
		command=>'ssh etdev1' },
	f4=> { title=>'etdev',
		command=>'ssh etdev' },
	f5=> { title=>'cvs/svn',
		command=>'ssh -tX etdev ssh -tX cvs.incentivelogic.com' },
	f6=> { title=>'Zod',
		command=>'ssh -tX etdev ssh -tX zod' },
	f7=> { title=>'ursa',
		command=>'ssh -X ursa.incentivelogic.com' },
	f8=> { title=>'Pluto2',
		command=>'ssh -X pluto2' },
	f9=> { title=>'Proxima',
		command=>'ssh -Xl eric proxima.generation-i.com' },
	f10=> { title=>'Pan',
		command=>'ssh -Xl eric pan.generation-i.com' },
	f11=> { title=>'Toy',
		command=>'ssh -Xl eric 192.168.15.44' },
	f12=> { title=>'jupiter',
		command=>'' },
	TERM_BASE=>$TERM{'rxvt'}
};
$servers{'saturn-gnome'}={
	f1=> { title=>'Rigel',
		command=>'rigel' },
	f2=> { title=>'OpenWrt',
		command=>'openwrt' },
	f3=> { title=>'ET Land',
		command=>'et-land' },
	f4=> { title=>'pi1',
		command=>'pi1' },
	f5=> { title=>'Virgo',
		command=>'virgo' },
	f6=> { title=>'AIY',
		command=>'aiy' },
	f7=> { title=>'Draco',
		command=>'draco' },
	f8=> { title=>'Lyra',
		command=>'lyra' },
	f9=> { title=>'MacBookPro',
		command=>'MacBookPro' },
	f10=> { title=>'Octopi',
		command=>'octopi' },
	f11=> { title=>'Phact',
		command=>'phact' },
	f12=> { title=>'saturn',
		command=>'Default' },
	TERM_BASE=>'/usr/bin/mate-terminal --geometry 80x40 --window --profile=[command]',
#	TERM_BASE=>$TERM{'gnome-terminal'}
};
$servers{'pluto-gnome'}={
    f1=> { title=>'Cygnus',
        command=>'cygnus' },
    f2=> { title=>'Phact',
        command=>'phact' },
    f3=> { title=>'etdev1',
        command=>'etdev1' },
    f4=> { title=>'etdev',
        command=>'etdev' },
    f5=> { title=>'cvs/svn',
        command=>'drcs001' },
    f6=> { title=>'et-debian',
        command=>'et-debian' },
    f7=> { title=>'Ursa',
        command=>'ursa' },
    f8=> { title=>'Pluto2',
        command=>'pluto2' },
    f9=> { title=>'Proxima',
        command=>'proxima' },
    f10=> { title=>'Pan',
        command=>'pan' },
    f11=> { title=>'jupiter',
        command=>'jupiter' },
    f12=> { title=>'pluto',
        command=>'' },
    TERM_BASE=>'/usr/bin/gnome-terminal --geometry 80x40 --tab-with-profile=[command]',
#    TERM_BASE=>$TERM{'gnome-terminal'}
};

$servers{'charon-gnome'}={
	f1=> { title=>'Bacon',
		command=>'bacon' },
	f2=> { title=>'Analytics',
		command=>'analytics' },
	f3=> { title=>'Analytics Dev',
		command=>'analytics-dev' },
	f4=> { title=>'gearman',
		command=>'gearman' },
	f5=> { title=>'cron',
		command=>'cron' },
	f6=> { title=>'Jam',
		command=>'jam' },
	f7=> { title=>'Stage',
		command=>'stage' },
	f8=> { title=>'',
		command=>'' },
	f9=> { title=>'Phact',
		command=>'phact' },
	f10=> { title=>'Lyra',
		command=>'lyra' },
	f11=> { title=>'Jupiter',
		command=>'jupiter' },
	f12=> { title=>'local',
		command=>'' },
	TERM_BASE=>'/usr/bin/gnome-terminal --geometry 80x40 --tab-with-profile=[command]',
};

$servers{'hydra-gnome'}={
	f1=> { title=>'Bacon',
		command=>'bacon' },
	f2=> { title=>'VM Dev',
		command=>'vm-dev' },
	f3=> { title=>'Ham',
		command=>'ham' },
	f4=> { title=>'Analytics Dev',
		command=>'analytics-dev' },
	f5=> { title=>'gearman',
		command=>'gearman' },
	f6=> { title=>'cron',
		command=>'cron' },
	f7=> { title=>'Jam',
		command=>'jam' },
	f8=> { title=>'Stage',
		command=>'stage' },
	f9=> { title=>'Phact',
		command=>'phact' },
	f10=> { title=>'Lyra',
		command=>'lyra' },
	f11=> { title=>'Jupiter',
		command=>'jupiter' },
	f12=> { title=>'local',
		command=>'' },
	f13=> { title=>'Analytics',
		command=>'analytics' },
	f14=> { title=>'vm-hub',
		command=>'vm-hub' },
	TERM_BASE=>'/usr/bin/mate-terminal --geometry 80x40 --window --profile=[command]',
};

$servers{'ixion-gnome'}={
	f1=> { title=>'Bacon',
		command=>'bacon' },
	f2=> { title=>'VM Dev',
		command=>'vm-dev' },
	f3=> { title=>'Ham',
		command=>'ham' },
	f4=> { title=>'Analytics Dev',
		command=>'analytics-dev' },
	f5=> { title=>'gearman',
		command=>'gearman' },
	f6=> { title=>'cron',
		command=>'cron' },
	f7=> { title=>'Jam',
		command=>'jam' },
	f8=> { title=>'Stage',
		command=>'stage' },
	f9=> { title=>'Phact',
		command=>'phact' },
	f10=> { title=>'Lyra',
		command=>'lyra' },
	f11=> { title=>'Saturn',
		command=>'saturn' },
	f12=> { title=>'local',
		command=>'' },
	f13=> { title=>'Analytics',
		command=>'analytics' },
	f14=> { title=>'vm-hub',
		command=>'vm-hub' },
	f15=> { title=>'Jupiter',
		command=>'jupiter' },
	TERM_BASE=>'/usr/bin/mate-terminal --geometry 80x40 --window --profile=[command]',
};

$servers{'saturn.generation-i.com'}=$servers{'saturn-gnome'};
$servers{'saturn'}=$servers{'saturn.generation-i.com'};
$servers{'jupiter.generation-i.com'}=$servers{'saturn-gnome'};
$servers{'jupiter'}=$servers{'saturn.generation-i.com'};

#$servers{'etdev'}=dclone($servers{'etdev.ilpoints.com'});
$servers{'etdev'}=dclone($servers{'etdev-gnome'});
$servers{'pluto'}=dclone($servers{'etdev.ilpoints.com'});

$servers{'pluto'}{TERM_BASE}=$TERM{'gnome-terminal'};
$servers{'pluto'}{f4}={ title=>'etdev',
		command=>'ssh etdev' };
$servers{'pluto'}{f12}={ title=>'pluto',
		command=>'' };
$servers{'pluto'}=$servers{'pluto-gnome'};
$servers{'pluto2'}={%{$servers{'pluto-gnome'}}};
$servers{'pluto2.incentivelogic.com'}={%{$servers{'pluto-gnome'}}};
$servers{'pluto2'}{f8}={ title=>'Pluto',
		command=>'pluto' };
$servers{'ethelin-desktop'}=dclone($servers{'ethelin-desktop-gnome'});
$servers{'charon'}={%{$servers{'charon-gnome'}}};
$servers{'hydra'}={%{$servers{'hydra-gnome'}}};
$servers{'ixion'}={%{$servers{'ixion-gnome'}}};
$servers{'draco'}={%{$servers{'ixion-gnome'}}};

# print $host."\n\n";
# use Data::Dumper; print Dumper(\%servers); # EKT
if(exists($servers{$host}) && exists($servers{$host}->{TERM_BASE}))
{
	$TERM_BASE=$servers{$host}->{TERM_BASE};
}
elsif(exists($servers{DEFAULT}) && exists($servers{DEFAULT}->{TERM_BASE}))
{
	$TERM_BASE=$servers{DEFAULT}->{TERM_BASE};
}

# print Dumper(\%servers);
# print Dumper($servers{$ARGV[0]});
# exit;

my @replace_tags;

my $working_base=$TERM_BASE;
while($working_base=~/\[([a-z0-9_]+?)\]/i)
{
	push(@replace_tags,$1);
	$working_base=~s/\[$1\]//g;
}

foreach my $arg (@ARGV) {
    foreach my $tag (@replace_tags)
    {
        # my $tag=$1;
        my $val;
        if (exists($servers{$host}) && exists($servers{$host}->{$arg})) {
            $val=$servers{$host}->{$arg}->{$tag};
        } elsif (exists($servers{DEFAULT}) && exists($servers{DEFAULT}->{$arg})) {
            $val=$servers{DEFAULT}->{$arg}->{$tag};
        } else {
            while (my ($key, $info) = each(%{$servers{$host}})) {
                if (ref($info) ne 'HASH' or !defined($info->{'title'})) {
                    next;
                }
                if (lc($arg) eq lc($info->{'title'})) {
                    $val = $info->{$tag};
                } elsif (lc($arg) eq lc($info->{'command'})) {
                    $val = $info->{$tag};
                } elsif (lc($arg) eq lc($key)) {
                    $val = $info->{$tag};
                }
            }
        }

        if (defined($val) && $val ne '') {
            $TERM_BASE=~s/\[$tag\]/$val/g;
        }
    }

    while ($TERM_BASE=~/(-([a-zA-Z]\s|-[^ =]+(=|\s))\s*\"?\[(command|title)\]\"?)(\s|$)/) {
        my $replace=quotemeta($1);

        $TERM_BASE=~s/$replace//;
    }

    print `$TERM_BASE &`;
    print $TERM_BASE."\n";
}
# print "host=$host\n";
