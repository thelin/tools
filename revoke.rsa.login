#!/bin/sh

KEY_TYPE=rsa

if [ -z "$1" ]
then
	echo Usage: $0 '<host>'
	echo Revoke passwordless logins from this account to '<host>' using RSA/DSA keys
	exit -1
fi

if [ ! -f ~/.ssh/id_$KEY_TYPE.pub ]
then
	echo There is no key to revoke
	exit -2
fi

ssh $1 "
if [ -f ~/.ssh/authorized_keys2 ]
then
	grep -v '"`cat ~/.ssh/id_$KEY_TYPE.pub`"' ~/.ssh/authorized_keys2 >~/.ssh/authorized_keys2.new

	if [ $? -ge 0 -o $? -eq 0 ]
	then
		if [ -s ~/.ssh/authorized_keys2.new ]
		then
			mv ~/.ssh/authorized_keys2.new ~/.ssh/authorized_keys2
		else
				rm -f ~/.ssh/authorized_keys2.new ~/.ssh/authorized_keys2
		fi
	else
		chmod 000 ~/.ssh/authorized_keys2.new
		echo 'Something strange happened so the new key isn'\''t available but can be viewed at ~/.ssh/authorized_keys2.new'
	fi
fi
"
