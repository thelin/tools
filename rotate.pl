#!/usr/bin/perl
#
# Log file rotator based on today's date
#
#


require 'getopts.pl';

sub usage {
print STDERR <<USAGE;  

Usage: $script [OPTIONS] logfile(s) ...
Rotate logfiles based on today's date
       -h                     help (this message)
       -a xx/xx/xx            rotate using alternate date
       -o dir                 output directory for rotated log file
       -c                     do not move origional
       -z                     gzip rotated file
       -d                     debug
       logfile(s)             file(s) to be rotated
       
USAGE
}

Getopts("a:o:hczd");

($script = $0) =~ s,.*/,,;

usage, exit if $opt_h || @ARGV < 1;

if(defined($opt_o))
{
  $outpath=$opt_o;
  if(!($outpath=~/\/$/))
    {$outpath=$outpath."/";}
}
else
  {$outpath="";}

if(defined($opt_a))
{
  if($opt_a=~/([0-9]+)\/([0-9]+)\/([0-9]+)/)
    {$datenum=$3.$1.$2;}
  else
  {
    print STDERR "Invalid Date: $opt_a\n";
    exit;
  }
  print "datenum=$datenum\n" if defined $opt_d;
  print "opt_a=$opt_a\n" if defined $opt_d;
}
else
{
  @date=localtime(time());
  $datenum=(1900+$date[5])*10000+($date[4]+1)*100+$date[3]; # format date to add zeros
	
}


print "outpath=$outpath\n" if defined $opt_d;
print "ARGVs=".scalar(@ARGV)."\n" if defined $opt_d;
for($x=0; $x<scalar(@ARGV); $x++)
{
  $file=$ARGV[$x];
  print "file=$file\n" if defined $opt_d;
  print "datenum=$datenum\n" if defined $opt_d;
	$pwd=`pwd`;
	chomp($pwd);
	print "ls=".`ls $pwd/$file` if defined $opt_d;
  if(!(-f "$pwd/$file"))
  {
    print STDERR "File Not Found: $pwd/$file\n";
  }
  else
  {
    if(-f "$outpath$file.$datenum" || 
       -f "$outpath$file.$datenum.gz")
    {
      $offset="01";
      while(-f "$outpath$file.$datenum$offset" || 
            -f "$outpath$file.$datenum$offset.gz")
      {
        $offset=$offset+1;
        if($offset<10 || length($offset)==1)
        {
          $offset="0".$offset;
        }
      }
      $datenum=$datenum.$offset;
    }
    print "offset=$offset\n" if defined $opt_d;
    #`mv $file $file.$datenum`;
    $file=~/([^\/]*)$/;
    $fn=$1;
    if(defined($opt_c))
      {`cp $file $outpath$fn.$datenum`;}
    else
      {`mv $file $outpath$fn.$datenum`;}
    
    #print "mv $file $outpath$fn.$datenum\n\n";
  
    sleep 2; #this will give all web processes that are currently writing to these files a chance to finish
    if(defined($opt_z))
    {
      `gzip $outpath$fn.$datenum`;
      $datenum=$datenum.".gz";
    }
    print "rotated to $outpath$fn.$datenum\n" if defined $opt_d;
  }
}
