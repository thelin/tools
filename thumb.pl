#!/usr/bin/perl

use strict;
use warnings;

use Getopt::Std;

my $option = process_options();

for my $arg (@ARGV)
{
	if($arg=~/^\./)
	{
		next;
	}
	if(-d $arg)
	{
		opendir(DIR, $arg);
		while(my $dir_file=readdir(DIR))
		{
			if($dir_file=~/^\./)
			{
				next;
			}
			$arg=~s#/$##;
			# print "adding $arg/$dir_file\n";
			push(@ARGV,$arg.'/'.$dir_file);
		}
		next;
	}
	
	if($arg!~/(jpg|png|gif|bmp)/i)
	{
		# print "skipping $arg\n";
		next;
	}
	
	my $cmd="convert ";
	my $out_file=$arg;
	
	if($option->{type})
	{
		$out_file=~s/\.(.{2,3})/.$option->{type}/;
	}
	if(!$option->{prefix})
	{
		$option->{prefix}='th_';
	}
	
	if($arg=~m#(^|/)$option->{prefix}# || -f $out_file)
	{
		next;
	}
	
	my $out_dir='';
	my $out_filename=$out_file;
	if($out_file=~m#^(.*)/([^/]*)$#)
	{
		$out_dir=$1.'/';
		$out_filename=$2;
	}
		
	$out_file=$out_dir.$option->{prefix}.$out_filename;
	
	
	my $size_x;
	my $size_y;
	if(!$option->{x})
	{
		$size_x=150;
	}
	else
	{
		$size_x=$option->{x};
	}
	if(!$option->{y})
	{
		$size_y=150;
	}
	else
	{
		$size_y=$option->{y};
	}
	
	$arg=~s/"/\"/s;
	$out_file=~s/"/\"/s;

	my $info=`identify "$arg"`;
	if($arg=~/gif$/ && $info=~/ ([0-9]+)c/)
	{
		$cmd.=" -colors $1";
	}
	
	if($info=~/ ([0-9]+)x([0-9]+)/)
	{
		if($1<$size_x && $2<$size_y)
		{
			`cp "$arg" "$out_file"`;
			next;
			$size_x=$1;
			$size_y=$2;
		}
	}
	

	$cmd.=" -geometry ".$size_x.'x'.$size_y;
	if($option->{force})
	{
		$cmd.='!';
	}
	
	$cmd.=' "'.$arg.'" "'.$out_file.'"';
	
	if($option->{dry_run})
	{
		print color("bold blue") . $cmd . color("reset") . "\n";
	}
	else
	{
		print `$cmd`;
	}
	
}



sub process_options {
      my %options = ();
  
      getopts('hx:y:fq:t:p:nr', \%options);
  
      if ($options{h}) { print usage(); exit(); }
  
      # strip whitespace
      foreach (keys %options) {
		        $options{$_} =~ s/^\s*//g;
		        $options{$_} =~ s/\s*$//g;
		    }
  
      my %map = (
		        x => 'x',
		        y => 'y',
		        q => 'quality',
		        t => 'type',
		        p => 'prefix',
		        f => 'force',
		        n => 'dry_run',
		        r => 'recursive',
		    );
      # map names
      foreach (keys %map) {
		        $options{$map{$_}} = $options{$_} if exists $options{$_};
		    }
  
      return \%options;
  }

 
sub usage {
  return "
Usage: $0 -h
	-h                  This help message
Optionals:
	-x <width>          Max Width
	-y <height>         Max Height
	-f                  Force Size
	-q <jpg quality>    JPEG Quality
	-t <type>           Force type (gif|jpg|png)
	-r                  Recursive
	-n                  Dry run
";
	
  }
