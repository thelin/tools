#!/usr/bin/perl -w

# figure out which state we're in
$out = `aticonfig --query-monitor`;
($monitor) = $out =~ /Enabled monitors: (\S+)/;

my $set_monitor;
if(defined($ARGV[0]) && $ARGV[0]=~/(both|internal|external)/)
{
	$set_monitor=$ARGV[0];
}
elsif ($monitor eq "crt1")
{
	$set_monitor='both';
}
elsif ($monitor eq "lvds")
{
	$set_monitor='external';
}
else
{
	$set_monitor='internal';
}

if ($set_monitor eq "both")
{
	print "Switching to both laptop panel and external LCD...\n";
	system('aticonfig --enable-monitor=crt1,lvds >/dev/null 2>&1');
	system('xrandr -s 1280x1024');
	# system('xsetpointer "<default pointer>"');
	# system('xmodmap -e "pointer = 1 6 2 7 8 4 5 3 9"');
}
elsif ($set_monitor eq "external")
{
	print "Switching to external LCD...\n";
	system('aticonfig --enable-monitor=crt1 >/dev/null 2>&1');
	system('xrandr -s 1280x1024');
	# system('xsetpointer "<default pointer>"');
	# system('xmodmap -e "pointer = 1 6 2 7 8 4 5 3 9"');
}
elsif ($set_monitor eq "internal")
{
	print "Switching to laptop panel...\n";
	system('aticonfig --enable-monitor=lvds >/dev/null 2>&1');
	system('xrandr -s 1400x1050');
	# system('xsetpointer "<default pointer>"');
	# system('xmodmap -e "pointer = 1 2 3 4 5 6 7 8 9"');
}

