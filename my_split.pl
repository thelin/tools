#!/usr/bin/perl 

use strict;
use warnings;

use Getopt::Std;
use Data::Dumper;

my $option = process_options();

if(scalar(@ARGV)==0)
{
	print "Missing file to split\n";
	print usage();
	exit;
}

my $file;
my $base;
if(scalar(@ARGV)==1)
{
	$file=$ARGV[0];
	$base=$file.'.';
}
elsif(scalar(@ARGV)==2)
{
	$file=$ARGV[0];
	$base=$ARGV[1];
}
else
{
	print "Missing file to split\n";
	print usage();
	exit;
}

if(!exists($option->{lines}))
{
	$option->{lines}=1000;
}

open(FILE,$file) || die "Unable to open $file\n";
my $count=0;
my $file_num=0;
my $header;
while(my $line=<FILE>)
{
	if($count==0)
	{
		$header=$line;
	}
	if($count%$option->{lines}==0)
	{
		my $out_file=$base.sprintf('%03d',$file_num);
		if($option->{suffix})
		{
			$out_file.='.'.$option->{suffix};
		}
		close(OUT);
		open(OUT, '>'.$out_file);
		$file_num++;
		print OUT $header;
		print "\nopening $out_file\n" if $option->{verbose};
		if($count==0)
		{
			$count++;
			next;
		}
	}
	print OUT $line;
	$count++;
	print "." if $option->{verbose} && $count%$option->{lines}%10==0;
}
print "\n$file_num files\t$count lines\n" if $option->{verbose};

sub process_options {
	my %options = ();

	getopts('hl:vs:', \%options);

	if ($options{h}) { print usage(); exit(); }

	# strip whitespace
	foreach (keys %options) {
		$options{$_} =~ s/^\s*//g;
		$options{$_} =~ s/\s*$//g;
	}

	my %map = (
		l => 'lines',		
		v => 'verbose',
		s => 'suffix',
	);
	# map names
	foreach (keys %map) {
		$options{$map{$_}} = $options{$_} if exists $options{$_};
	}
	
	return \%options;
}


sub usage {
	return <<"HERE"
Usage: $0 -h <big_file> [<output_base>]
 -h             This help message
Optionals:
 -l <lines>     Number of lines to include in each file (default: 1000)
 -s <suffix>    Suffix to append to output files
 -v             Show verbose output

HERE

}
