git status |grep -E 'modified:|new file:|deleted:' >/dev/null 2>&1
if [ $? -eq 0 ]
then
    git stash
    git rebase -i
    echo "Press CTRL-C to abort stash restore or enter to continue"
    read && git stash pop
else
    git rebase -i
fi
