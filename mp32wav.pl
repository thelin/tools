#!/usr/bin/perl -w

# convert mp3 files into wav files

if(!defined($ARGV[0]))
{
	($script = $0) =~ s,.*/,,;
	print "Usage: $script <mp3_directory>\n";
	exit(-1);
}

$args=join " ",@ARGV;
@files=`find "$args"`;

foreach $mp3 (@files)
{
	chomp($mp3);
	# print $mp3."\n";
	# next;
	$wav=$mp3;
	$wav=~s/(mp3|ogg)$/wav/i;
	if(-f $mp3 && ! -f $wav)
	{
		$wav=~s/([^a-z0-9\/.])/\\$1/ig;
		$mp3=~s/([^a-z0-9\/.])/\\$1/ig;
		
		if($mp3=~/mp3$/i)
		{
			print "mpg123 -w $wav $mp3\n";
			`mpg123 -w $wav $mp3`;
		}
		else
		{
			print "ogg123 -d wav -f $wav $mp3\n";
			`ogg123 -d wav -f $wav $mp3`;
		}
	}
}
