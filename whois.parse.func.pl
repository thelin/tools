#!/usr/bin/perl -w
sub print_whois
{
	my ($dat);
	$dat = $_[0];
	print "SITE: 		'" . $dat->{"site"} . "'\n";
	print "COMPANY: 	'" . $dat->{"company"} . "'\n";
	print "ADDRESS: 	'" . $dat->{"address"} . "'\n";
	print "CITY: 		'" . $dat->{"city"} . "'\n";
	print "STATE: 		'" . $dat->{"state"} . "'\n";
	print "ZIP: 		'" . $dat->{"zip"} . "'\n";
	print "COUNTRY: 	'" . $dat->{"country"} . "'\n";
	print "ADMIN NAME: 	'" . $dat->{"admin name"} . "'\n";
	print "ADMIN EMAIL: 	'" . $dat->{"admin email"} . "'\n";
	print "ADMIN HANDLE: 	'" . $dat->{"admin handle"} . "'\n";
	print "ADMIN PHONE:	'" . $dat->{"admin phone"} . "'\n";
	print "ADMIN FAX:	'" . $dat->{"admin fax"} . "'\n";
	print "TECH NAME: 	'" . $dat->{"tech name"} . "'\n";
	print "TECH EMAIL: 	'" . $dat->{"tech email"} . "'\n";
	print "TECH HANDLE: 	'" . $dat->{"tech handle"} . "'\n";
	print "TECH PHONE: 	'" . $dat->{"tech phone"} . "'\n";
	print "TECH FAX: 	'" . $dat->{"tech fax"} . "'\n";
	print "BILLING NAME: 	'" . $dat->{"billing name"} . "'\n";
	print "BILLING EMAIL: 	'" . $dat->{"billing email"} . "'\n";
	print "BILLING HANDLE:	'" . $dat->{"billing handle"} . "'\n";
	print "BILLING PHONE:	'" . $dat->{"billing phone"} . "'\n";
	print "BILLING FAX:	'" . $dat->{"billing fax"} . "'\n";
	print "NICUPDATE: 	'" . $dat->{"nicupdate"} . "'\n";
	print "NICCREATE: 	'" . $dat->{"niccreate"} . "'\n";
	print "QUERYDATE: 	'" . $dat->{"querydate"} . "'\n";
}

sub whois_query
{
	my ( $x, $out, $dom, $max_retrys, $redo, $tmp_dom );
	( $dom, $max_retrys ) = @_;
	$redo       = 0;
	$retry      = 0;
	$out        = "";
	$server     = "whois.internic.net";
	$max_retrys = 5 if !$max_retrys;
	$opt_v      = 0 if !$opt_v;
	do
	{
		$redo = 0;    # clear redo status
		if ( $dom =~ /\// && -f $dom )
		{
			open( IN, $dom );
			$/     = undef;
			$whois = <IN>;
			close(IN);
			if ( $whois =~ /Domain Name:\s*(.*)/i ) { $dom = $1; }

			#print "whois=$whois\n";
			print "\ndom=$dom\n";
		}
		elsif ( defined($tmp_dom) )
		{
			$whois = whois( "'!$tmp_dom'", $server, $timeout );
		}
		else
		{
			$whois = whois( "$dom", $server, $timeout );

			#print "--[$whois]--\n";
			#print "%%[$dom]%%\n";
		}
		print "whois=$whois\n" if $opt_v > 2;
		if ( $whois =~ /Whois Server: ([A-Z0-9-.]+)/i )
		{
			$server = $1;
			$redo   = 1;
			print "server=$server\n" if $opt_v > 0;
		}
		elsif (    $whois =~ /To single out one record/
				&& $whois =~ /\(([A-Z0-9-.]*-DOM)\)[\s]*$dom/ )
		{
			$tmp_dom = $1;
			$redo    = 1;
			print "Singling Out $tmp_dom\n" if !$opt_q;
		}
		else
		{

			#print "^@^@^@^@\n@\n";
			#print "[$whois](($dom))\n";
			if ( $whois =~ /Domain Name: $dom/i )
			{

			# this needs to be after the 'Domain Name' has been found because it
			# may be in the region that is to be stripped.
				$whois =~ s/The InterNIC Registration Services.*$//s;
				$whois =~ s/^.*Registrant:\s*\n//s;
				$dbup = parse_whois_data( $dom, $whois );
				print "dbup=$dbup\n" if $opt_v > 2;
				$redo = 0;
				$out  = "$dbup";
			}
			else
			{
				if ( $whois =~ /No Match/i )
				{
					$retry++;
					$redo  = -1;
					$error = "No match found for $dom";

					#print "No match for $dom <$retry>\n" if !$opt_q;
				}
				elsif ( $whois =~
/(Please wait a few minutes and try again|Please wait a while and try again|WHOIS database is down|No route to host)/
					|| $whois eq "" )
				{
					$retry++;
					print "Retrying <$retry>\n" if !$opt_q;
					print "whois=$whois\n";
					$redo  = 1;
					$error = "Max retrys reached without success.";
				}
				else
				{
					$error = "Unknown parsing error";
					print "{$whois}\n" if $opt_v > 0;
					$retry++;
					$redo = 1;
					return ($out);
				}
			}
		}
		if ( $retry >= $max_retrys || $redo == -1 )
		{
			return ( "\nError: " . $error . "\n\n" );
		}

		#print "~@@~server=$server\n";
	} while ( $redo > 0 );
	undef $tmp_dom;
	$retry = 0;

	#$out;
	$dbup;
}

sub parse_whois_data
{
	local ( $site, $company, $address, $city, $state, $zip, $country,
			%admin_contact );
	my ( %tech_contact, %billing_contact, $update, $create, $query_date, $dns );
	my ( $who,          $out,             $x,      $error );

	#$file=$_[0];
	$who     = $_[1];
	$error   = $site = $out = "";
	$company = $address = $city = $state = $zip = $country = "";

	#$name=$email="";
	$update = $create = $query_date = "";

	#	$dns1name = $dns1ip = $dns2name = $dns2ip = "";
	%admin_contact = %tech_contact = %billing_contact = (
														  "name"   => "",
														  "handle" => "",
														  "email"  => "",
														  "phone"  => "",
														  "fax"    => ""
	);

	#print "who=[[[$who]]]\n";
	$who =~ s/'/\\'/g;
	@whois = split( /\n/, $who );
	$count = @whois;
	if    ( $whois[0] =~ /(.*) \(.*\)/ ) { $company = $1; }
	elsif ( $whois[0] =~ /\s*(.*)/ )     { $company = $1; }

	#print "whois[0]=".$whois[0]."\n";
	undef(@dns);
	$x = 0;
	while ( $x <= $count && defined( $whois[$x] ) )
	{
		if ( $whois[$x] =~ /Domain Name:\s*([a-zA-Z0-9-.]*)/ )
		{

			#$domain=$1;
			$site = $1;
			$addr = $who;
			$addr =~
			  s/^.*\n//;    # remove the first line (which is the company name)
			$addr =~ s/\n\n.*$//s;    # chop at the first blank line
			$addr =~ s/^\s*//mg;      # remove all leading spaces
			( $address, $city, $state, $zip, $country ) = parse_address($addr);
		}
		elsif ( $whois[$x] =~
				/Record created on ([0-9]{1,2})-([a-zA-Z]{3})-([0-9]{4})/i )
		{
			$create = "$3-" . replace_months($2) . "-$1";
		}
		elsif ( $whois[$x] =~
				/Record expires on ([0-9]{1,2})-([a-zA-Z]{3})-([0-9]{4})/i )
		{
			$expires = "$3-" . replace_months($2) . "-$1";
		}
		elsif ( $whois[$x] =~
			   /Record last updated on ([0-9]{1,2})-([a-zA-Z]{3})-([0-9]{4})/i )
		{
			$update = "$3-" . replace_months($2) . "-$1";
		}
		elsif ( $whois[$x] =~
			 /Database last updated on ([0-9]{1,2})-([a-zA-Z]{3})-([0-9]{4})/i )
		{
			$query_date = "$3-" . replace_months($2) . "-$1";
		}
		elsif ( $whois[$x] =~
				/Created on: ([0-9]{1,2})-([a-zA-Z]{3})-([0-9]{2,4})/i )
		{
			$year = $3;
			if ( $year < 100 )
			{
				if ( $year > 38 ) { $year = "19" . $year; }
				else { $year = "20" . $year; }
			}
			$create = "$year-" . replace_months($2) . "-$1";
		}
		elsif ( $whois[$x] =~
				/Last Updated on: ([0-9]{1,2})-([a-zA-Z]{3})-([0-9]{2,4})/ )
		{
			$year = $3;
			if ( $year < 100 )
			{
				if ( $year > 38 ) { $year = "19" . $year; }
				else { $year = "20" . $year; }
			}
			$update = "$year-" . replace_months($2) . "-$1";
		}
		elsif ( $whois[$x] =~
				/Expires on: ([0-9]{1,2})-([a-zA-Z]{3})-([0-9]{2,4})/ )
		{
			$year = $3;
			if ( $year < 100 )
			{
				if ( $year > 38 ) { $year = "19" . $year; }
				else { $year = "20" . $year; }
			}
			$expires = "$year-" . replace_months($2) . "-$1";
		}
		elsif ( $whois[$x] =~ / Contact/ )
		{
			$contact_lines = "";
			for ( $cnt = 1 ;
				  $whois[ $x + $cnt ] !~ / Contact:/ && $cnt < 7 ;
				  $cnt++ )
			{
				$contact_lines .= $whois[ $x + $cnt ] . "\n";
			}

			# $qa=$whois[$x+3].$whois[$x+4].$whois[$x+5].$whois[$x+6],
			if (    $whois[ $x + 2 ] !~ /\(FAX\)/
				 && $whois[ $x + 3 ] =~ /^Fax- (.+)/ )
			{
				$whois[ $x + 2 ] = $whois[ $x + 2 ] . " (FAX) " . $1;
				$whois[ $x + 3 ] = "";
			}
			if ( $whois[$x] =~ /Administrative Contact[A-Za-z, ]*:/ )
			{
				%admin_contact =
				  parse_contact( $whois[ $x + 1 ], $contact_lines );
			}
			if ( $whois[$x] =~ /Technical Contact[A-Za-z, ]*:/ )
			{
				%tech_contact =
				  parse_contact( $whois[ $x + 1 ], $contact_lines );
			}
			if ( $whois[$x] =~ /Billing Contact[A-Za-z, ]*:/ )
			{
				%billing_contact =
				  parse_contact( $whois[ $x + 1 ], $contact_lines );
			}
		}
		elsif ( $whois[$x] =~ /Domain servers in listed order:/ )
		{
			$d = 1;
			while ( $x <= $count && defined( $whois[ $x + 1 ] ) )
			{
				$x++;
				if ( $whois[$x] =~
					 /([a-zA-Z0-9.-]+)[^0-9]+([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)/ )
				{
					push( @dns, { "name" => $1, "ip" => $2 } );
					$d++;
				}
			}
		}
		$x++;
	}

	#if($query_date eq "0000-00-00")
	#{
	#$query_date=$today;
	#}
	if ( $create eq "0000-00-00" )
	{
		print "create=$create";
	}
	if ( $update eq "0000-00-00" )
	{
		print "update=$update";
	}
	if ( $error eq "" )
	{

# make sure that all single quotes are escaped and data only contains valid characters
		$company =~ s/'/\\'/g;
		$address =~ s/'/\\'/g;
		$city    =~ s/'/\\'/g;
		$state   =~ s/[^A-Z]//g;
		$zip     =~ s/[^0-9-]//g;
		$country =~ s/'/\\'/g;
		foreach $key ( keys %admin_contact )
		{
			$admin_contact{$key} =~ s/'/\\'/g;
		}
		foreach $key ( keys %tech_contact )
		{
			$tech_contact{$key} =~ s/'/\\'/g;
		}
		foreach $key ( keys %billing_contact )
		{
			$billing_contact{$key} =~ s/'/\\'/g;
		}

		#validate email addresses
		$tech_contact{"email"}    =~ s/[^0-9-A-Za-z.@!]//g;
		$admin_contact{"email"}   =~ s/[^0-9-A-Za-z.@!]//g;
		$billing_contact{"email"} =~ s/[^0-9-A-Za-z.@!]//g;
		$update                   =~ s/[^0-9-]//g;
		$create                   =~ s/[^0-9-]//g;
		$query_date               =~ s/[^0-9-]//g;
		$out = "SITE: 		'$site'\n";
		$out = $out . "COMPANY: 	'$company'\n";
		$out = $out . "ADDRESS: 	'$address'\n";
		$out = $out . "CITY: 		'$city'\n";
		$out = $out . "STATE: 		'$state'\n";
		$out = $out . "ZIP: 		'$zip'\n";
		$out = $out . "COUNTRY: 	'$country'\n";
		$out = $out . "ADMIN NAME: 	'" . $admin_contact{"name"} . "'\n";
		$out = $out . "ADMIN EMAIL: 	'" . $admin_contact{"email"} . "'\n";
		$out = $out . "ADMIN HANDLE: 	'" . $admin_contact{"handle"} . "'\n";
		$out = $out . "ADMIN PHONE:	'" . $admin_contact{"phone"} . "'\n";
		$out = $out . "ADMIN FAX:	'" . $admin_contact{"fax"} . "'\n";
		$out = $out . "TECH NAME: 	'" . $tech_contact{"name"} . "'\n";
		$out = $out . "TECH EMAIL: 	'" . $tech_contact{"email"} . "'\n";
		$out = $out . "TECH HANDLE: 	'" . $tech_contact{"handle"} . "'\n";
		$out = $out . "TECH PHONE: 	'" . $tech_contact{"phone"} . "'\n";
		$out = $out . "TECH FAX: 	'" . $tech_contact{"fax"} . "'\n";
		$out = $out . "BILLING NAME: 	'" . $billing_contact{"name"} . "'\n";
		$out = $out . "BILLING EMAIL: 	'" . $billing_contact{"email"} . "'\n";
		$out = $out . "BILLING HANDLE:	'" . $billing_contact{"handle"} . "'\n";
		$out = $out . "BILLING PHONE:	'" . $billing_contact{"phone"} . "'\n";
		$out = $out . "BILLING FAX:	'" . $billing_contact{"fax"} . "'\n";
		$out = $out . "NICUPDATE: 	'$update'\n";
		$out = $out . "NICCREATE: 	'$create'\n";
		$out = $out . "QUERYDATE: 	'$query_date'\n";
		for ( $x = 0 ; $x < scalar(@dns) ; $x++ )
		{
			$out = $out . "DNS NAME[$x]: 	'" . $dns[$x]{"name"} . "'\n";
			$out = $out . "DNS IP[$x]: 	'" . $dns[$x]{"ip"} . "'\n";
		}

		#"DNS1NAME: 	'$dns1name'\n".
		#"DNS1IP: 	'$dns1ip'\n".
		#"DNS2NAME: 	'$dns2name'\n".
		#"DNS2IP: 	'$dns2ip'\n";
		$hash = {
				  "site"           => "$site",
				  "company"        => "$company",
				  "address"        => "$address",
				  "city"           => "$city",
				  "state"          => "$state",
				  "zip"            => "$zip",
				  "country"        => "$country",
				  "admin name"     => $admin_contact{"name"},
				  "admin email"    => $admin_contact{"email"},
				  "admin handle"   => $admin_contact{"handle"},
				  "admin phone"    => $admin_contact{"phone"},
				  "admin fax"      => $admin_contact{"fax"},
				  "tech name"      => $tech_contact{"name"},
				  "tech email"     => $tech_contact{"email"},
				  "tech handle"    => $tech_contact{"handle"},
				  "tech phone"     => $tech_contact{"phone"},
				  "tech fax"       => $tech_contact{"fax"},
				  "billing name"   => $billing_contact{"name"},
				  "billing email"  => $billing_contact{"email"},
				  "billing handle" => $billing_contact{"handle"},
				  "billing phone"  => $billing_contact{"phone"},
				  "billing fax"    => $billing_contact{"fax"},
				  "nicupdate"      => "$update",
				  "niccreate"      => "$create",
				  "querydate"      => "$query_date"
		};
		for ( $x = 0 ; $x < scalar(@dns) ; $x++ )
		{
			$hash->{ "dns" . ( $x + 1 ) . "name" } = $dns[$x]{"name"};
			$hash->{ "dns" . ( $x + 1 ) . "ip" }   = $dns[$x]{"ip"};
		}
	}
	else
	{
		return ("Error: $error");
	}

	#$out;
	$hash;
}

sub replace_months
{
	local ($search) = lc( $_[0] );
	local ( %month, $count, $abrev, $mn );

	#%month={"jan"=>"01","feb"=>"02","mar"=>"03","apr"=>"04","may"=>"05",
	#"jun"=>"06","jul"=>"07","aug"=>"08","sep"=>"09","oct"=>"10",
	#"nov"=>"11","dec"=>"12"};
	$month{"jan"} = "01";
	$month{"feb"} = "02";
	$month{"mar"} = "03";
	$month{"apr"} = "04";
	$month{"may"} = "05";
	$month{"jun"} = "06";
	$month{"jul"} = "07";
	$month{"aug"} = "08";
	$month{"sep"} = "09";
	$month{"oct"} = "10";
	$month{"nov"} = "11";
	$month{"dec"} = "12";

	#print "\n{{$search";
	while ( ( $abrev, $mn ) = each(%month) )
	{
		if ( $search eq "" )
		{
			last;
		}
		if ( length($search) == 3 && $search =~ /$abrev/i )
		{
			$search =~ s/$abrev/$mn/i;

			#print "^";
			last;
		}
		if ( $search =~ /$mn/i )
		{
			$search =~ s/$mn/$abrev/i;

			#print "@";
			last;
		}

		#print "($search!=$abrev) ";
	}

	#print " - $search}}\n";
	$search;
}

sub country_lookup
{
	local ($search) = $_[0];
	local ( %country, $count, $abrev, $cntry, $found );
	$country{"Andorra"}                        = "AD";
	$country{"United Arab Emirates"}           = "AE";
	$country{"Afghanistan"}                    = "AF";
	$country{"Antigua and Barbuda"}            = "AG";
	$country{"Anguilla"}                       = "AI";
	$country{"Albania"}                        = "AL";
	$country{"Armenia"}                        = "AM";
	$country{"Netherlands Antilles"}           = "AN";
	$country{"Angola"}                         = "AO";
	$country{"Antarctica"}                     = "AQ";
	$country{"Argentina"}                      = "AR";
	$country{"American Samoa"}                 = "AS";
	$country{"Austria"}                        = "AT";
	$country{"Australia"}                      = "AU";
	$country{"Aruba"}                          = "AW";
	$country{"Azerbaidjan"}                    = "AZ";
	$country{"Bosnia-Herzegovina"}             = "BA";
	$country{"Barbados"}                       = "BB";
	$country{"Bangladesh"}                     = "BD";
	$country{"Belgium"}                        = "BE";
	$country{"Burkina Faso"}                   = "BF";
	$country{"Bulgaria"}                       = "BG";
	$country{"Bahrain"}                        = "BH";
	$country{"Burundi"}                        = "BI";
	$country{"Benin"}                          = "BJ";
	$country{"Bermuda"}                        = "BM";
	$country{"Brunei Darussalam"}              = "BN";
	$country{"Bolivia"}                        = "BO";
	$country{"Brazil"}                         = "BR";
	$country{"Bahamas"}                        = "BS";
	$country{"Bhutan"}                         = "BT";
	$country{"Bouvet Island"}                  = "BV";
	$country{"Botswana"}                       = "BW";
	$country{"Belarus"}                        = "BY";
	$country{"Belize"}                         = "BZ";
	$country{"Canada"}                         = "CA";
	$country{"Cocos Islands"}                  = "CC";
	$country{"Central African Republic"}       = "CF";
	$country{"Congo"}                          = "CG";
	$country{"Switzerland"}                    = "CH";
	$country{"Ivory Coast"}                    = "CI";
	$country{"Cook Islands"}                   = "CK";
	$country{"Chile"}                          = "CL";
	$country{"Cameroon"}                       = "CM";
	$country{"China"}                          = "CN";
	$country{"Colombia"}                       = "CO";
	$country{"Costa Rica"}                     = "CR";
	$country{"Former Czechoslovakia"}          = "CS";
	$country{"Cuba"}                           = "CU";
	$country{"Cape Verde"}                     = "CV";
	$country{"Christmas Island"}               = "CX";
	$country{"Cyprus"}                         = "CY";
	$country{"Czech Republic"}                 = "CZ";
	$country{"Germany"}                        = "DE";
	$country{"Djibouti"}                       = "DJ";
	$country{"Denmark"}                        = "DK";
	$country{"Dominican Republic"}             = "DO";
	$country{"Dominica"}                       = "DM";
	$country{"Algeria"}                        = "DZ";
	$country{"Ecuador"}                        = "EC";
	$country{"Estonia"}                        = "EE";
	$country{"Egypt"}                          = "EG";
	$country{"Western Sahara"}                 = "EH";
	$country{"Eritrea"}                        = "ER";
	$country{"Spain"}                          = "ES";
	$country{"Ethiopia"}                       = "ET";
	$country{"Finland"}                        = "FI";
	$country{"Fiji"}                           = "FJ";
	$country{"Falkland Islands"}               = "FK";
	$country{"Micronesia"}                     = "FM";
	$country{"Faroe Islands"}                  = "FO";
	$country{"France"}                         = "FR";
	$country{"Gabon"}                          = "GA";
	$country{"Great Britain"}                  = "GB";
	$country{"Grenada"}                        = "GD";
	$country{"Georgia"}                        = "GE";
	$country{"French Guyana"}                  = "GF";
	$country{"Ghana"}                          = "GH";
	$country{"Gibraltar"}                      = "GI";
	$country{"Greenland"}                      = "GL";
	$country{"Gambia"}                         = "GM";
	$country{"Guinea"}                         = "GN";
	$country{"Guadeloupe"}                     = "GP";
	$country{"Equatorial Guinea"}              = "GQ";
	$country{"Greece"}                         = "GR";
	$country{"S. Georgia & S. Sandwich Isls."} = "GS";
	$country{"Guatemala"}                      = "GT";
	$country{"Guam"}                           = "GU";
	$country{"Guinea Bissau"}                  = "GW";
	$country{"Guyana"}                         = "GY";
	$country{"Hong Kong"}                      = "HK";
	$country{"Heard and McDonald Islands"}     = "HM";
	$country{"Honduras"}                       = "HN";
	$country{"Croatia"}                        = "HR";
	$country{"Haiti"}                          = "HT";
	$country{"Hungary"}                        = "HU";
	$country{"Indonesia"}                      = "ID";
	$country{"Ireland"}                        = "IE";
	$country{"Israel"}                         = "IL";
	$country{"India"}                          = "IN";
	$country{"British Indian Ocean Territory"} = "IO";
	$country{"Iraq"}                           = "IQ";
	$country{"Iran"}                           = "IR";
	$country{"Iceland"}                        = "IS";
	$country{"Italy"}                          = "IT";
	$country{"Jamaica"}                        = "JM";
	$country{"Jordan"}                         = "JO";
	$country{"Japan"}                          = "JP";
	$country{"Kenya"}                          = "KE";
	$country{"Kyrgyzstan"}                     = "KG";
	$country{"Cambodia"}                       = "KH";
	$country{"Kiribati"}                       = "KI";
	$country{"Comoros"}                        = "KM";
	$country{"Saint Kitts & Nevis Anguilla"}   = "KN";
	$country{"Korea"}                          = "KR";
	$country{"North Korea"}                    = "KP";
	$country{"South Korea"}                    = "KR";
	$country{"Kuwait"}                         = "KW";
	$country{"Cayman Islands"}                 = "KY";
	$country{"Kazakhstan"}                     = "KZ";
	$country{"Laos"}                           = "LA";
	$country{"Lebanon"}                        = "LB";
	$country{"Saint Lucia"}                    = "LC";
	$country{"Liechtenstein"}                  = "LI";
	$country{"Sri Lanka"}                      = "LK";
	$country{"Liberia"}                        = "LR";
	$country{"Lesotho"}                        = "LS";
	$country{"Lithuania"}                      = "LT";
	$country{"Luxembourg"}                     = "LU";
	$country{"Latvia"}                         = "LV";
	$country{"Libya"}                          = "LY";
	$country{"Morocco"}                        = "MA";
	$country{"Monaco"}                         = "MC";
	$country{"Moldavia"}                       = "MD";
	$country{"Madagascar"}                     = "MG";
	$country{"Marshall Islands"}               = "MH";
	$country{"Macedonia"}                      = "MK";
	$country{"Mali"}                           = "ML";
	$country{"Myanmar"}                        = "MM";
	$country{"Mongolia"}                       = "MN";
	$country{"Macau"}                          = "MO";
	$country{"Northern Mariana Islands"}       = "MP";
	$country{"Martinique"}                     = "MQ";
	$country{"Mauritania"}                     = "MR";
	$country{"Montserrat"}                     = "MS";
	$country{"Malta"}                          = "MT";
	$country{"Mauritius"}                      = "MU";
	$country{"Maldives"}                       = "MV";
	$country{"Malawi"}                         = "MW";
	$country{"Mexico"}                         = "MX";
	$country{"Malaysia"}                       = "MY";
	$country{"Mozambique"}                     = "MZ";
	$country{"Namibia"}                        = "NA";
	$country{"New Caledonia"}                  = "NC";
	$country{"Niger"}                          = "NE";
	$country{"Norfolk Island"}                 = "NF";
	$country{"Nigeria"}                        = "NG";
	$country{"Nicaragua"}                      = "NI";
	$country{"Netherlands"}                    = "NL";
	$country{"The Netherlands"}                = "NL";
	$country{"Norway"}                         = "NO";
	$country{"Nepal"}                          = "NP";
	$country{"Nauru"}                          = "NR";
	$country{"Neutral Zone"}                   = "NT";
	$country{"Niue"}                           = "NU";
	$country{"New Zealand"}                    = "NZ";
	$country{"Oman"}                           = "OM";
	$country{"Panama"}                         = "PA";
	$country{"Peru"}                           = "PE";
	$country{"Polynesia"}                      = "PF";
	$country{"Papua New Guinea"}               = "PG";
	$country{"Philippines"}                    = "PH";
	$country{"Pakistan"}                       = "PK";
	$country{"Poland"}                         = "PL";
	$country{"Saint Pierre and Miquelon"}      = "PM";
	$country{"Pitcairn Island"}                = "PN";
	$country{"Puerto Rico"}                    = "PR";
	$country{"Portugal"}                       = "PT";
	$country{"Palau"}                          = "PW";
	$country{"Paraguay"}                       = "PY";
	$country{"Qatar"}                          = "QA";
	$country{"Reunion (French)"}               = "RE";
	$country{"Romania"}                        = "RO";
	$country{"Russian Federation"}             = "RU";
	$country{"Rwanda"}                         = "RW";
	$country{"Saudi Arabia"}                   = "SA";
	$country{"Solomon Islands"}                = "SB";
	$country{"Seychelles"}                     = "SC";
	$country{"Sudan"}                          = "SD";
	$country{"Sweden"}                         = "SE";
	$country{"Singapore"}                      = "SG";
	$country{"Saint Helena"}                   = "SH";
	$country{"Slovenia"}                       = "SI";
	$country{"Svalbard and Jan Mayen Islands"} = "SJ";
	$country{"Slovak Republic"}                = "SK";
	$country{"Sierra Leone"}                   = "SL";
	$country{"San Marino"}                     = "SM";
	$country{"Senegal"}                        = "SN";
	$country{"Somalia"}                        = "SO";
	$country{"Suriname"}                       = "SR";
	$country{"Saint Tome and Principe"}        = "ST";
	$country{"Former USSR"}                    = "SU";
	$country{"El Salvador"}                    = "SV";
	$country{"Syria"}                          = "SY";
	$country{"Swaziland"}                      = "SZ";
	$country{"Turks and Caicos Islands"}       = "TC";
	$country{"Chad"}                           = "TD";
	$country{"French Southern Territories"}    = "TF";
	$country{"Togo"}                           = "TG";
	$country{"Thailand"}                       = "TH";
	$country{"Tadjikistan"}                    = "TJ";
	$country{"Tokelau"}                        = "TK";
	$country{"Turkmenistan"}                   = "TM";
	$country{"Tunisia"}                        = "TN";
	$country{"Tonga"}                          = "TO";
	$country{"East Timor"}                     = "TP";
	$country{"Turkey"}                         = "TR";
	$country{"Trinidad and Tobago"}            = "TT";
	$country{"Tuvalu"}                         = "TV";
	$country{"Taiwan"}                         = "TW";
	$country{"Tanzania"}                       = "TZ";
	$country{"Ukraine"}                        = "UA";
	$country{"Uganda"}                         = "UG";
	$country{"United Kingdom"}                 = "UK";
	$country{"England"}                        = "UK";
	$country{"USA Minor Outlying Islands"}     = "UM";
	$country{"United States"}                  = "US";
	$country{"United States of America"}       = "US";
	$country{"USA"}                            = "US";
	$country{"Uruguay"}                        = "UY";
	$country{"Uzbekistan"}                     = "UZ";
	$country{"Vatican City State"}             = "VA";
	$country{"Saint Vincent & Grenadines"}     = "VC";
	$country{"Venezuela"}                      = "VE";
	$country{"Virgin Islands (British)"}       = "VG";
	$country{"Virgin Islands (USA)"}           = "VI";
	$country{"Vietnam"}                        = "VN";
	$country{"Vanuatu"}                        = "VU";
	$country{"Wallis and Futuna Islands"}      = "WF";
	$country{"Samoa"}                          = "WS";
	$country{"Yemen"}                          = "YE";
	$country{"Mayotte"}                        = "YT";
	$country{"Yugoslavia"}                     = "YU";
	$country{"South Africa"}                   = "ZA";
	$country{"Zambia"}                         = "ZM";
	$country{"Zaire"}                          = "ZR";
	$country{"Zimbabwe"}                       = "ZW";
	$search_nodots                             = $search;
	$search_nodots =~ s/\.//g;
	$count = %country;

	#print "\$search_nodots=$search_nodots\t[$count]\n";
	undef($found);
	if ( $search eq "" ) { $found = 1; }
	$x = 0;
	while ( ( ( $cntry, $abrev ) = each(%country) ) && !defined($found) )
	{

		#print ":";
		if ( length($search) == 2 && $search =~ /$abrev/i )
		{
			$search =~ s/$abrev/$cntry/i;
			$found = 2;
		}
		elsif ( $search =~ /\b$cntry\b/i )
		{
			$search =~ s/\b$cntry\b/$abrev/i;

			#print "country found=$search\t($cntry,$abrev)\n";
			$found = 3;
		}
		elsif ( $search_nodots =~ /\b$cntry\b/i )
		{
			$search_nodots =~ s/$cntry/$abrev/i;
			$search = $search_nodots;
			$found  = 4;
		}
		elsif ( $search_nodots =~ /^$abrev$/i
		  ) #this must match the whole string or else it will match when it shouldn't
		{
			$search = $search_nodots;
			$found  = 5;
		}
		$x++;
	}

	#rint "country[count=$count]<=-$search=-=>[x=$x]:$found:country\n";
	$search;
}

sub state_lookup
{
	local ($search) = $_[0];
	local ( %state, $count, $abrev, $st, $found );
	$state{"Alabama"}          = "AL";
	$state{"Ala"}              = "AL";
	$state{"Alaska"}           = "AK";
	$state{"Arizona"}          = "AZ";
	$state{"Arkansas"}         = "AR";
	$state{"California"}       = "CA";
	$state{"Calif"}            = "CA";
	$state{"Calif."}           = "CA";
	$state{"Cal"}              = "CA";
	$state{"Colorado"}         = "CO";
	$state{"Colo"}             = "CO";
	$state{"Connecticut"}      = "CT";
	$state{"Conn"}             = "CT";
	$state{"Delaware"}         = "DE";
	$state{"Del"}              = "DE";
	$state{"Dist of Columbia"} = "DC";
	$state{"D.C."}             = "DC";
	$state{"Florida"}          = "FL";
	$state{"Fla"}              = "FL";
	$state{"Fla."}             = "FL";
	$state{"Georgia"}          = "GA";
	$state{"Hawaii"}           = "HI";
	$state{"Idaho"}            = "ID";
	$state{"Illinois"}         = "IL";
	$state{"Ill"}              = "IL";
	$state{"Indiana"}          = "IN";
	$state{"Iowa"}             = "IA";
	$state{"Kansas"}           = "KS";
	$state{"Kentucky"}         = "KY";
	$state{"Louisiana"}        = "LA";
	$state{"Maine"}            = "ME";
	$state{"Maryland"}         = "MD";
	$state{"Massachusetts"}    = "MA";
	$state{"Mass"}             = "MA";
	$state{"Michigan"}         = "MI";
	$state{"Mich"}             = "MI";
	$state{"Minnesota"}        = "MN";
	$state{"Mississippi"}      = "MS";
	$state{"Missouri"}         = "MO";
	$state{"Montana"}          = "MT";
	$state{"Nebraska"}         = "NE";
	$state{"Nevada"}           = "NV";
	$state{"New Hampshire"}    = "NH";
	$state{"N.H."}             = "NH";
	$state{"New Jersey"}       = "NJ";
	$state{"N.J."}             = "NJ";
	$state{"N.J"}              = "NJ";
	$state{"New Mexico"}       = "NM";
	$state{"N.M."}             = "NM";
	$state{"New York"}         = "NY";
	$state{"N.Y."}             = "NY";
	$state{"N.Y"}              = "NY";
	$state{"North Carolina"}   = "NC";
	$state{"N.C."}             = "NC";
	$state{"North Dakota"}     = "ND";
	$state{"N.D."}             = "ND";
	$state{"Ohio"}             = "OH";
	$state{"Oklahoma"}         = "OK";
	$state{"Okla"}             = "OK";
	$state{"Oregon"}           = "OR";
	$state{"Pennsylvania"}     = "PA";
	$state{"Rhode Island"}     = "RI";
	$state{"R.I."}             = "RI";
	$state{"South Carolina"}   = "SC";
	$state{"S.C."}             = "SC";
	$state{"South Dakota"}     = "SD";
	$state{"S.D."}             = "SD";
	$state{"Tennessee"}        = "TN";
	$state{"Tenn"}             = "TN";
	$state{"Texas"}            = "TX";
	$state{"Tex"}              = "TX";
	$state{"Utah"}             = "UT";
	$state{"Vermont"}          = "VT";
	$state{"Virginia"}         = "VA";
	$state{"Washington"}       = "WA";
	$state{"West Virginia"}    = "WV";
	$state{"W.V."}             = "WV";
	$state{"Wisconsin"}        = "WI";
	$state{"Wis"}              = "WI";
	$state{"Wyoming"}          = "WY";
	$state{"Puerto Rico"}      = "PR";
	$state{"P.R."}             = "PR";
	$search_nodots             = $search;
	$search_nodots =~ s/\.//g;
	if ( $search eq "" ) { $found = 1; }

	#rint "(state)\$search_nodots=$search_nodots\n";
	$x     = 0;
	$count = @state;
	undef($found);
	while ( ( ( $st, $abrev ) = each(%state) ) && !defined($found) )
	{

		#print "($search) {$st,$abrev}";
		if ( $search =~ /,\s*$st\s+/i )
		{

			#print "--{$abrev}--";
			$search =~ s/(,\s*)$st(\s+)/$1$abrev$2/i;
			$found = 1;
		}

		#print "-=-=-=-($search) {$st,$abrev}";
		if ( $search =~ /\s+$st\s+/i )
		{

			#print "=={$abrev}==";
			$search =~ s/(\s+)$st(\s+)/,$1$abrev$2/i;
			$found = 1;
		}
		elsif ( $search =~ /\s+$abrev\s+/i
				&& !( $search =~ /,\s*$abrev\s+/i ) )
		{

			#print "comma{$abrev}comma\n";
			$search =~ s/(\s+)$abrev(\s+)/,$1$abrev$2/i;
			$search =~ s/\s+,/,/;
			$found = 1;
		}
		elsif ( $st =~ /(.*) (.*)/ )
		{
			$first  = $1;
			$second = $2;
			if ( $search =~ /$first,\s*$second\s+/i )
			{

				#print "**{$abrev}**";
				$search =~ s/\s+$first(,\s*)$second(\s+)/$1$abrev$2/i;
				$found = 1;
			}
			elsif ( $search =~ /$second,\s*$first\s+/i )
			{

				#print "&&{$abrev}&&";
				$search =~ s/\s+$second(,\s*)$first(\s+)/$1$abrev$2/i;
				$found = 1;
			}
		}
		elsif ( $search_nodots =~ /\s+$abrev\s+/i
				&& !( $search =~ /,\s*$abrev\s+/i ) )
		{

			#print "comma{$abrev}comma\n";
			$search_nodots =~ s/(\s+)$abrev(\s+)/,$1$abrev$2/i;
			$search_nodots =~ s/\s+,/,/;
			$search = $search_nodots;
			$found  = 1;
		}
		elsif ( $search_nodots =~ /\W$st\W/i
		  ) #this must match use word boundries or else it will match when it shouldn't
		{
			$search_nodots =~ s/(\W)$st(\W)/$1$abrev$2/i;

			#print "@@@<$search_nodots>@@@\n";
			$search = $search_nodots;
			$found  = 1;
		}
		elsif ( $search_nodots =~ /\W$abrev\W/i )
		{
			$search_nodots =~ s/(\W)$abrev(\W)/$1$abrev$2/i;

			#print "%%%<$search_nodots>%%%\n";
			$search = $search_nodots;
			$found  = 1;
		}

		#print "%^%^%^%($search) {$st,$abrev}\n";
		$x++;
	}

	#print "search!@#=$search=#@!search\n";
	$search;
}

sub parse_contact
{
	my (%out);
	@lines = split /\n/, $_[1];

	#print "\$_[0]=".$_[0]."\n";
	#print "\$_[1]=".$_[1]."\n";
	if (
		$_[0] =~ /\s*(.+)  \(([a-z0-9]+)\)  ([a-z0-9.-]+\@[a-z0-9.-]+)/i ) #name
	{
		$out{"name"}   = $1;
		$out{"handle"} = $2;
		$out{"email"}  = $3;
		$out{"name"}  =~ s/\s*$//;
		$out{"email"} =~ s/\s*$//;
	}
	elsif ( $_[0] =~ /\s*(.+)  ([a-z0-9.-]+\@[a-z0-9.-]+)/i )              #name
	{
		$out{"name"}  = $1;
		$out{"email"} = $2;
		$out{"name"}  =~ s/\s*$//;
		$out{"email"} =~ s/\s*$//;
	}

	#else
	#{
	#if($_[0]=~/\s*(.+)\s+\(.+/)		#name
	#{
	#$out{"name"}=$1;
	#$out{"name"}=~s/\s*$//;
	#}
	#if($_[0]=~/.+\((.+)\).+/)			#handle
	#{
	#$out{"handle"}=$1;
	#}
	#if($_[0]=~/.+\) +(.+)/)				#email
	#{
	#$out{"email"}=$1;
	#}
	#}
	foreach $line (@lines)
	{
		$line =~ s/^\s+//;
		if ( $line =~ /\s*([^A-Z]+)\s+\(FAX\)\s+(.+)/i )
		{
			$out{"phone"} = $1;
			$out{"fax"}   = $2;

			#print "@@ ".$out{"phone"}."~~".$out{"fax"}."  {$line}\n";
		}
		elsif ( $line =~
				/\s*([^A-Z]+?[^A-Z0-9]+[0-9]+)\s+([^A-Z]+[^A-Z0-9]+[0-9]+)/i )
		{
			$out{"phone"} = $1;
			$out{"fax"}   = $2;

			#print "!! ".$out{"phone"}."~~".$out{"fax"}."  {$line}\n";
		}
		elsif ( $line =~ /\s*([^A-Z]+)/i )
		{
			$out{"phone"} = $1;
			$out{"fax"}   = "";

			#print "## ".$out{"phone"}."~~".$out{"fax"}."  {$line}\n";
		}
	}
	%out;
}

sub parse_address
{
	local ( @lines, $tmp, $count, $x );
	@lines = split( /\n/, $_[0] );
	$count = @lines - 1;

	#print "\n$_[0]\n";
	#print " count=$count-=-=".$lines[$count]."\n";
	if ( $lines[$count] =~ /^([a-zA-Z]{2})$/ )
	{
		$country = uc($1);
		$count--;
	}
	else
	{
		undef $tmp;
		$tmp = country_lookup( $lines[$count] );

		#print "country_lookup($lines[$count])=".country_lookup($lines[$count]);
		#print "\ttmp=$tmp\n";
		if ( $tmp =~ /^([a-zA-Z]{2})$/ )
		{

			#print "-=-=-=-=-=-=-=-=-=$1=-=-=-=-=-=\n";
			$country = uc($1);
			$count--;
		}
	}

	#print " count=$count-=-=".$lines[$count]."\n";
	if ( $country eq "" )
	{
		$country = "US";
	}
	$address = $lines[0];
	if ( $country eq "US" )
	{
		undef $tmp;
		$tmp = state_lookup( $lines[$count] );

		#print " count=$count-=-=".$lines[$count]."\n";
		if (    $lines[$count] =~ /(.*),\s*([A-Za-z]{2})\W\s*(.*)$/
			 || $tmp =~ /(.*),\s*([A-Za-z]{2})\W\s*(.*)$/ )
		{
			$city  = $1;
			$state = uc($2);
			$zip   = $3;

			#print "&& $city ^^ $state ## $zip &&\n";
			$count--;
		}
		else
		{
			print "------------[][$count]-" . $lines[$count] . "-[][]\n";
		}
		for ( $y = 1 ; $y < $count ; $y++ )
		{
			$address = $address . "\n" . $lines[$x];
		}
		if ( $city eq "" || $state eq "" )
		{
			print "\ntmp[$x]=$tmp\n";
			$error = "Bad address ($tmp)";
		}
	}
	else
	{
		$city = $lines[$count];
		for ( $x = 1 ; $x < $count ; $x++ )
		{
			$address = $address . "\n" . $lines[$x];
		}
	}

#print "address=$address\ncity=$city\nstate=$state\nzip=$zip\ncountry=$country\n";
	( $address, $city, $state, $zip, $country );
}

sub whois
{
	local ( $domain, $server, $timeout, $result );
	( $domain, $server, $timeout ) = @_;
	if ( !defined($domain) || $domain eq "" ) { return; }
	if ( !defined($server) || $server eq "" )
	{
		$server = "whois.internic.net";
	}
	if ( !defined($timeout) || $timeout eq "" ) { $timeout = 30; }
	return ( timeout_cmd( "/usr/bin/whois $domain\@$server", $timeout ) );
}

sub timeout_cmd
{
	local ( $cmd, $timeout, $result );
	( $cmd, $timeout ) = @_;
	if ( !defined($timeout) || $timeout eq "" ) { $timeout = 30; }
	eval {
		local $SIG{ALRM} = sub { die "alarm\n" };    # NB \n required
		alarm $timeout;
		$result = `$cmd`;

		#print "/usr/bin/whois $domain\@$server\n\n";
		alarm 0;
	};
	die if $@ && $@ ne "alarm\n";                    # propagate errors
	if ($@)
	{
		return ("");
	}
	else
	{
		return ($result);
	}
}
1;
