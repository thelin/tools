#!/usr/bin/perl -w

use File::Basename;
use Cwd;

sub sort_func
{
  ($a1=$a)=~s/$arg\./$arg/;
  ($b1=$b)=~s/$arg\./$arg/;
  $b1 cmp $a1;
}

foreach $arg (@ARGV)
{
  $dir=dirname($arg);
  opendir(DIR,$dir);
  @files=sort sort_func grep {/$arg/} readdir(DIR);
  while($file=shift(@files))
  {
    if($file=~/\.[0-9]+$/)
    {
      next;
    }
    if($file=~/$arg(\.?)([0-9]+)(\.(gz|bz2|zip))?$/)
    {
      if($1) {$dot=$1;} else {$dot="";}
      $digit=$2;
      if($3) {$ext=$3;} else {$ext="";}
      rename($arg.$dot.$digit.$ext,$arg.$dot.($digit+1).$ext);
      #print "rename(".$arg.$dot.$digit.$ext.",".$arg.$dot.($digit+1).$ext.")\n";
    }
    elsif($file eq $arg)
    {
      rename($arg,$arg.".1");
    }
    else
    {
      print "file=$file\n";
    }
  }
}
					
					
