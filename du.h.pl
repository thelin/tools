#!/usr/bin/perl

use strict;
use warnings;

my $du;
if ( -f "/usr/bin/du" )
{
	$du="/usr/bin/du";
}
elsif ( -f "/bin/du" )
{
	$du="/bin/du";
}
else
{
	die "Unable to find 'du'\n";
}
	
if(scalar(@ARGV)==0)
{
	$ARGV[0]='*';
}

my @data;
my $args='';
for my $arg (@ARGV)
{
	if($arg ne '*')
	{
		$arg=~s/([^a-z0-9_-])/\\$1/g;
	}
	$args.=$arg.' ';
}
my @tmp=`$du -sk -- $args`;
@data=(@data, @tmp);

my $total=0;
for my $line (sort { ($a =~ /^(\d+)/)[0] <=> ($b =~ /^(\d+)/)[0] }  @data)
{
	if(my ($num, $name)=$line=~/^([0-9]+)\s(.*)/)
	{
		$total+=$num;
		printf("%7s\t%s\n",num_to_human($num),$name);
	}
}
if(scalar(@data)>1)
{
	print "-------------------------------\n";
	printf("%7s\ttotal\n",num_to_human($total));
}

sub num_to_human
{
	my $num=shift;
	my $new_num;
	$num*=1024;
	if($num>1024*1024*1024)
	{	
		$new_num=sprintf("%.2fG",($num/(1024*1024*1024)));
		$new_num=~s/([0-9]{3,})\.[0-9]{2}([A-Z])$/$1$2/;
	}
	elsif($num>1024*1024)
	{
		$new_num=sprintf("%.2fM",($num/(1024*1024)));
		$new_num=~s/([0-9]{3,})\.[0-9]{2}([A-Z])$/$1$2/;
	}
	elsif($num>1024)
	{
		$new_num=sprintf("%.2fK",($num/(1024)));
		$new_num=~s/([0-9]{3,})\.[0-9]{2}([A-Z])$/$1$2/;
	}
	else
	{
		return '';
	}
	return $new_num;
}
