#!/bin/sh

KEY_TYPE=rsa
KEY_TYPE_ALT=dsa

if [ ! -f ~/.ssh/id_$KEY_TYPE.pub -a -f ~/.ssh/id_$KEY_TYPE_ALT.pub ]
then
	KEY_TYPE=KEY_TYPE_ALT
fi


if [ -z "$1" ]
then
	echo Usage: $0 '<host>'
	echo Grant passwordless logins from this account to '<host>' using RSA/DSA keys
	exit -1
fi

if [ ! -f ~/.ssh/id_$KEY_TYPE.pub ]
then
	ssh-keygen -t $KEY_TYPE -N '' -f ~/.ssh/id_$KEY_TYPE
fi

cat ~/.ssh/id_$KEY_TYPE.pub|ssh $1 "
if [ ! -d ~/.ssh ];
then
	mkdir ~/.ssh
fi

chmod 700 ~/.ssh/

cat>>~/.ssh/authorized_keys2
chmod 600 ~/.ssh/authorized_keys2

if [ ! -f ~/.ssh/id_$KEY_TYPE ]
then
	ssh-keygen -t $KEY_TYPE -N '' -f ~/.ssh/id_$KEY_TYPE
fi
"
