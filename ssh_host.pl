#!/usr/bin/perl

use warnings;
use strict;
use Data::Dumper;

my $debug;

my %host_map=(
	'jupiter'=> {
		'192\.168\.15'=>'ssh 192.168.15.244',
		''=>'ssh -tX jupiter.generation-i.com',
	},
	'apollo'=>{
		'192\.168\.15'=>'ssh 192.168.15.100',
		''=>'ssh -tX jupiter.generation-i.com ssh -tX apollo',
	},
	'eris'=>{
		'192\.168\.15'=>'ssh 192.168.15.70',
		''=>'ssh -tX jupiter.generation-i.com ssh -tX eris',
	},
	'rip1'=>{
		'192\.168\.15'=>'ssh rip1',
		''=>'ssh -tX jupiter.generation-i.com ssh -tX rip1',
	},
	'rip2'=>{
		'192\.168\.15'=>'ssh rip2',
		''=>'ssh -tX jupiter.generation-i.com ssh -tX rip2',
	},
	'pan'=>{
		'192\.168\.15'=>'ssh 192.168.15.101',
		''=>'ssh -tX jupiter.generation-i.com ssh -tX pan',
	},
	'pluto'=>{
		'192\.168\.15'=>'ssh 192.168.15.15',
		'192\.168\.1[01]1'=>'ssh -X 192.168.101.154',
	},
	'pluto2'=>{
		'192\.168\.15'=>'ssh 192.168.15.16',
		'192\.168\.1[01]1'=>'ssh -X 192.168.101.154',
	},
	'etdev'=>{
		'192\.168\.(15|101|111)'=>'ssh -tX 192.168.101.212',
	},
);
my $ips=`/sbin/ifconfig|grep 'inet addr:'|cut -d: -f2|cut -d' ' -f1`;

if($debug)
{
	use Data::Dumper; warn Dumper(\%host_map); # EKT
	use Data::Dumper; warn Dumper($ips); # EKT
	print "\n\n";
}
if(scalar(@ARGV)==0)
{
	die "Usage: $0 <host>\n";
}

if(defined($host_map{$ARGV[0]}))
{
	for my $regex (keys(%{$host_map{$ARGV[0]}}))
	{
		if($regex eq '')
		{
			next;
		}
		if($ips=~/$regex/)
		{
			if($debug)
			{
				print 'FOUND '.$regex.'='.$host_map{$ARGV[0]}{$regex}."\n";
			}
			else
			{
				system($host_map{$ARGV[0]}{$regex});
				exit;
			}
		}
		print $regex.'='.$host_map{$ARGV[0]}{$regex}."\n" if $debug;
	}
	my $default=$host_map{$ARGV[0]}{''};
	if(!$default)
	{
		warn "No default set for this host.\n";
	}
	else
	{
		if($debug)
		{
			print "\n\n" if $debug;
			warn Dumper($default) if $debug; # EKT
		}
		else
		{
			system($default);
		}
	}
}
