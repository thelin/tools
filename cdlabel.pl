#!/usr/bin/perl -w

if($ARGV[0])
	{ $dev=$ARGV[0]; }
else
	{ $dev="/dev/cdrom"; }

my $max_attempts = 5;

my $attempts;
for($attempts=0; $attempts<$max_attempts; $attempts++)
{
	if(open(CD,$dev))
	{
		last;
	}
	sleep 2;
}
if($attempts>=$max_attempts)
{
	die "Tried and failed to read the volume label\n";
}

read CD,$line,32808; # skip beginning
read CD,$line,30;
# print "find=".index($line,"Misc2")."\n";
# print "--[".$line."]--(".length($line).")\n";

$line=~s/(^\s*|\s*$)//g;
# print "--[".$line."]--(".length($line).")\n";
print $line."\n";
