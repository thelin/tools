#!/usr/bin/perl
use POSIX;

#
# Nist.pl to keep your system time up to date by using time servers.
# Copyright (C) 1998, 1999, 2000 Ali Onur Cinar <root@zdo.com>
#
# Latest version can be downloaded from:
#
#  http://www.zdo.com
#   ftp://hun.ece.drexel.edu/pub/cinar/nist*
#   ftp://ftp.cpan.org/pub/CPAN/authors/id/A/AO/AOCINAR/nist*
#   ftp://sunsite.unc.edu/pub/Linux/system/admin/timei/nist*
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version. And also
# please DO NOT REMOVE my name, and give me a CREDIT when you use
# whole or a part of this program in an other program.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

use Socket qw(PF_INET SOCK_STREAM AF_INET);


########################## user configuration ###########################
@timeserver = ('time.nist.gov',
	                   'time-a.nist.gov',
	                   'time-b.nist.gov',
	                   'time-nw.nist.gov',
	                   'time-a.timefreq.bldrdoc.gov',
	                   'time-b.timefreq.bldrdoc.gov',
	                   'time-c.timefreq.bldrdoc.gov',
	                   'utcnist.colorado.edu',
	                   'nist1.datum.com');
#	                   'utcnist1.reston.mci.net',

# $timeserver = 'time_a.timefreq.bldrdoc.gov';	# time server
$port = '13';					                        # time port (default:13)
$timediff = '-07:00:00';			                # time differance
$datepr = '/bin/date';				                # full path of date command
#########################################################################


for($tries=0; $tries<10; $tries++)
{
	$timeserver=$timeserver[rand(scalar(@timeserver))];

	
	
	
	$timediff =~/(.)(..).(..).(..)/g;
	$diff = (($2*3600)+($3*60)+$4);
	$diff = "$1$diff";
	
	if ($> ne 0)
	{
		print STDERR "This program should run as root user to be able to update sytem date.\n";
		exit 1;
	}
	
	if ($timeserver =~ /^(\d+)\.(\d+)\.(\d+)\.(\d+)$/)
	{
		$timeserver_addr = pack('C4', $1, $2, $3, $4);
	} else {
		$timeserver_addr = gethostbyname($timeserver);
	}
	
	print "Connecting to $timeserver on port $port...\n";
	
	if (!socket(NIST, AF_INET, SOCK_STREAM, getprotobyname("tcp") || 6))
	{
		print "socket failed ($!)\n"; exit;;
	}
	
	if (!connect(NIST, pack('Sna4x8', AF_INET, $port, $timeserver_addr)))
	{
		print "connect to $timeserver failed ($!)\n";
		next;
		# exit 1;
	}
	
	while (<NIST>)
	{
		$time_data_raw = $_;
		last if ( /NIST/);
	}
	
	close NIST;
	
# $time_data_raw="52136 01-08-15 23:59:59 50 0 0  95.6 UTC(NIST) *";
# $time_data_raw="52136 01-08-16 00:00:00 50 0 0  95.6 UTC(NIST) *";
	
	if($time_data_raw =~ /.{6}(..).(..).(..).(..).(..).(..)/g)
	{
		my ($raw_year,$raw_month,$raw_day,$raw_hour,$raw_min,$raw_sec);
		# print "time_data_raw=$time_data_raw\n";
		# print "Current GMT is $1-$2-$3 $4:$5:$6\n";
		
		$raw_year=$1;
		$raw_month=$2;
		$raw_day=$3;
		$raw_hour=$4;
		$raw_min=$5;
		$raw_sec=$6;
		
		if($1>38)
	  {$raw_yr=$1;}
		else
  	{$raw_yr=$1+100;}
		
		$time=mktime($6,$5,$4,$3,($2-1),$raw_yr);
		# print "GMT timestamp=$time\n";
		$time+=$diff;
		# print "MST timestamp=$time\n";
	}
	else
	{
		print STDERR "Bad date format from server\n";
		exit 2
	}
	
	($sec,$min,$hour,$day,$month,$year,undef,undef,undef)=localtime($time);
	if($year>=100)
  {$year-=100;}
  
	printf "Local time is  %02d-%02d-%02d %02d:%02d:%02d\n", $year, $month+1, $day, $hour, $min, $sec;
	print "Updating the system time. ";
	$date_command = sprintf ("%s %02d%02d%02d%02d20%02d.%02d",$datepr,$month+1, $day, $hour,$min, $year, $sec);
	system ($date_command);
	if($?!=0)
	{
		print STDERR "Error: unable to set date\n";
		print STDERR $date_command."($?)\n";
		exit 3;
	}
	# print `date`;
	`date >> /tmp/nist`;
	if($tries > 0)
	{
		print STDERR "Successful after $tries tries\n";
	}
	exit;
}
print STDERR "Tryed $tries times unsuccessfully\n";
exit -1;
