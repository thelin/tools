#!/usr/bin/env python2
"""ADB Sqlite.

Usage:
     adbsqlite.py [options]

Options:
    -h --help                      Show this screen.
    -v --version                   Show version.
    -D <device> --device=<device>  adb device id (only needed when more than one device is connected) [default: {last_device}]
    -a <app> --app=<app>           App package [default: {last_package}]
    -d <db> --db=<db>              Database file [default: {last_db_file}]
    -t <dir> --tmpdir=<dir>        Dir for temp files [default: {tmpdir}]

"""

CONFIG = {
    'last_device': 'foo',
    'last_db_file': 'foo.db',
    'last_package': 'com.example',
    'tmpdir': '/tmp'
}

# if this errors run: pip install docopt
from docopt import docopt

import ConfigParser
import os
from subprocess import check_call
import subprocess
import tempfile

def main(arguments):
    deviceArg = ""
    if hasattr(arguments, "--device") and arguments["--device"]:
        deviceArg = "-s " + arguments['--device']
        
    dbFile = arguments['--db']
    appPackage = arguments['--app']
    temp = tempfile.NamedTemporaryFile(suffix='.db',
                                prefix='adb_',
                                dir=arguments['--tmpdir'],
                                )

    
    remoteFile = "/data/data/" + appPackage + "/databases/" + dbFile

    try:
        check_call(filter(None, ["adb", deviceArg, "run-as " + appPackage + " chmod 666 " + remoteFile]))
        check_call(filter(None, ["adb", deviceArg, "pull", remoteFile, temp.name]))
    except subprocess.CalledProcessError:
        exit()
    infoPre = os.stat(temp.name)
    try:
        check_call(["sqlite3", temp.name])
    except subprocess.CalledProcessError:
        print "sqlite3 failed..."
        exit()

    infoPost = os.stat(temp.name)
    if infoPre.st_mtime != infoPost.st_mtime:
        while True:
            var = raw_input("Change made to " + dbFile + ". Do you want to upload them to the device? (Y/n): ")
            if  not var or var[0].lower() == "y":
                try:
                    check_call(filter(None, ["adb", deviceArg, "push", temp.name, remoteFile]))
                    break
                except subprocess.CalledProcessError:
                    exit()
            elif var[0].lower() == "n":
                break
    

def config():
    Config = ConfigParser.ConfigParser()

    #print os.getenv("HOME") + "/.adbsqlite.rc"
    Config.read(os.getenv("HOME") + "/.adbsqlite.rc")

    # add the settings to the structure of the file, and lets write it out...
    if not Config.has_section("DEFAULTS"):
        Config.add_section("DEFAULTS")

    arguments = docopt(__doc__.format(**dict(Config.items("DEFAULTS"))), version="0.00001")
    cfgfile = open(os.getenv("HOME") + "/.adbsqlite.rc",'w')


    Config.set("DEFAULTS", "last_device", arguments["--device"])
    Config.set("DEFAULTS", "last_package", arguments["--app"])
    Config.set("DEFAULTS", "last_db_file", arguments["--db"])
    Config.set("DEFAULTS", "tmpdir", arguments["--tmpdir"])
    Config.write(cfgfile)
    cfgfile.close()

    return arguments




if __name__ == '__main__':
    args = config()
    main(args)

