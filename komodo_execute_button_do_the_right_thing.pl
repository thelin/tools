#!/usr/bin/perl

use strict;
use warnings;
use Cwd;

#use Data::Dumper; warn '@ARGV='.Dumper(\@ARGV);

my ($file, $directory, $project) = @ARGV;
chdir($directory);

#my $cwd = getcwd;
#print "file = $file\n";
#print "dir = $directory\n";
#print "project = $project\n";
#print "cwd = $cwd\n";

$directory ||= '';
my $exit = 0;

my $project_dir;
if($directory=~m#^(.*/(workspace|lw(_local)?)/([^/]+)(-[^/]+)?/)(.*)#)
{
    #print "1=$1\n";
    #print "2=$2\n";
    #print "3=$3\n";
    $project_dir = $1;
}
else
{
	die "Unable to determine project directory\n";
}

if(not $file or not -f $file)
{
    print "File not found giving up\n";
    exit -1;
}

if($file=~/\.(pl|sh)/ and not -x $file)
{
	print "making $file executable\n";
	chmod 0755, $file;
}

my $do_restart = 1;
if(-f 'Makefile')
{
    print "Making Chunks...\n";
    my $out = `make -s 2>&1`;
    if($out =~ m#/usr/sbin/apachectl graceful: httpd gracefully restarted#)
    {
        $do_restart = 0;
        $out =~ s#/usr/sbin/apachectl graceful: httpd gracefully restarted\n##;
    }
    if($out=~/\[fail\] (.*) \(.* errors\)/)
    {
        my $out2 = `$project_dir/misc/scripts/chunk_validate -v $1`;
        iprint(4, $out2);
        $do_restart = 0;
    }
    else
    {
        $out=~s/^\s*\[ ok \] (.*)\n//gm;
        iprint(2, $out);
    }

}
elsif( -x $file )
{
	print "running $file\n";
	my $out = `./$file 2>&1`;
	if($? == 0) {
		$out .= ' :)';
	}
	else
	{
		$exit--;
	}
	iprint(2, $out);
	#print "out = $out";
	$do_restart = 0;
}
elsif($file=~/\.(conf|cfg)$/ and $directory=~m#/clients/.*/scripts#)
{
	print "running Roll script\n";
	my $out = `./roll.sh 2>&1`;
	iprint(2, $out);
	$do_restart = 0;
}

if($do_restart)
{
    if($file=~/Dispatcher/)
    {
        if( -f '~/workspace/devs/ethelin/misc/apache.restart')
        {
            my $out = `~/workspace/devs/ethelin/misc/apache.restart 2>&1`;
            iprint(2, $out);
			$exit--;
        }
        else
        {
            my $out = `sudo /etc/init.d/apache stop;sleep 5;sudo /etc/init.d/apache start 2>&1`;
            if($out !~ m#/etc/init.d/apache stop: httpd stopped.*/etc/init.d/apache start: httpd started#s)
            {
                iprint(2, $out);
				$exit--;
            }
            else
            {
                print "  Full Restart :)\n";
            }
        }
    }
	elsif($directory=~m#/lw(_local)/#)
	{
        print "reloading app servers\n";
        my $out = `ssh dev3.int ~/lw/lwscripts/dev/apps reload 2>&1`;
        if($out =~ m#Reloading.*:\s\[\s*OK\s*\]#)
        {
            print "  app servers reloaded :)\n";
        }
        else
        {
            iprint(2,$out);
			$exit--;
        }
	}
	elsif($directory=~m#/lw(_local)/# and 0)
	{
		# TODO: determine proper app
		my $app = 'billing';
        print "restarting $app\n";
        my $out = `ssh dev3.int ~/lw/lwscripts/dev/apps restart $app 2>&1`;
        if($out =~ m#Starting $app:\s\[\s*OK\s*\]#)
        {
            print "  $app restarted :)\n";
        }
        else
        {
            iprint(2,$out);
			$exit--;
        }
	}
    else
    {
        print "doing a graceful\n";
        my $out = `sudo -n /usr/sbin/apachectl graceful 2>&1`;
        if($out =~ m#/usr/sbin/apachectl graceful: httpd gracefully restarted#)
        {
            print "  Graceful :)\n";
        }
        else
        {
            iprint(2,$out);
			$exit--;
        }
    }
}
print "Done";

exit $exit;

sub iprint
{
    my ($num_spaces, $string) = @_;

    my $spaces = '';
    for (0..$num_spaces)
    {
        $spaces.=' ';
    }

    my @lines = split /\n/, $string;
    for my $line (@lines)
    {
        print $spaces.$line."\n";
    }
    return;
}
