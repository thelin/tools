#!/usr/bin/perl -w

# convert a list of ogg files to mp3s retaining the tag info

use strict;
use Getopt::Std;
use Data::Dumper;

my $option = process_options();

my %tag_cmds=('title'=>'--song=',
	'artist'=>'--artist=',
	'album'=>'--album=',
	'date'=>'--year=',
	'comment'=>'--comment=',
	'tracknumber'=>'--track=',
	'genre'=>'--genre=',
	);
	
for my $arg (@ARGV)
{
	print $arg."-\n";
	my @ogg_info_out=`ogginfo "$arg"`;
	if ($? != 0)
	{
		die "Error: $arg is not an ogg file\n";
	}
	
	my $path;
	my $file;
	
	if($arg=~/(.*)\/([^\/]*)$/)
	{
		$path=$1;
		$file=$2;
	}
	
	my %ogg_info;
	my $tag_opts='';
	for my $line (@ogg_info_out)
	{
		if($line=~/(title|artist|genre|date|album|tracknumber|comment)=(.*)/)
		{
			$ogg_info{$1}=$2;
		}
	}
	
	my $out_file;
	if($ogg_info{artist})
	{
		$out_file=sprintf('%s - %s %s.mp3',
			$ogg_info{artist},
			$ogg_info{tracknumber},$ogg_info{title});
	}
	else
	{
		$out_file=$arg;
		$out_file=~s/ogg$/mp3/i;
		if($out_file=~/(.+) - (([0-9]+) )?(.+)/)
		{
			$ogg_info{artist}=$1;
			$ogg_info{tracknumber}=$3;
			$ogg_info{title}=$4;
		}
		
	}
	
	for my $field (keys(%ogg_info))
	{
		my $value=$ogg_info{$field};
		# print "$field=$value --- ".$tag_opts."\n";
		$value=~s/([^a-zA-Z0-9_])/\\$1/g;
		# $value=~s/\'/\\\'/g;
		$tag_opts.=sprintf(" %s%s",$tag_cmds{$field},$value);
	}
	$out_file=~s/([^a-zA-Z0-9_])/\\$1/g;
	if($path)
	{
		$out_file=$path.'/'.$out_file;
	}
	my $cmd="oggdec \"$arg\" -o - |lame -h -v -V 3 - \"$out_file\"";
	if($option->{verbose} or $option->{dry_run})
	{
		print color("bold blue") . $cmd . color("reset") . "\n";
	}
	if(!$option->{dry_run})
	{
		print $cmd;
	}
	
	$cmd="id3tag $tag_opts $out_file";
	if($option->{verbose} or $option->{dry_run})
	{
		print color("bold blue") . $cmd . color("reset") . "\n";
	}
	if(!$option->{dry_run})
	{
		print $cmd;
	}
	
}

sub process_options
{
	my %options = ();
	
	getopts('hnv:', \%options);
	
	if ($options{h}) { print usage(); exit(); }
	
	# strip whitespace
	foreach (keys %options) {
		$options{$_} =~ s/^\s*//g;
		$options{$_} =~ s/\s*$//g;
	}
	
	my %map = (
		n => 'dry_run',
		v => 'verbose',
	);
	# map names
	foreach (keys %map) {
		$options{$map{$_}} = $options{$_} if exists $options{$_};
	}
	
	return \%options;
}


sub usage {
	return <<"HERE"
Usage: $0 -h
  -h          This help message
Optionals:
  -n          Dry run
  -v <level>  Verbose
HERE
		
}
