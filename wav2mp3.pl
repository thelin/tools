#!/usr/bin/perl -w

if(! -f "/usr/bin/lame")
{
	die("Unable to find lame");
}

my @cmds;

foreach my $arg (@ARGV)
{
	if($arg=~/\.wav$/)
	{
		my $mp3_file=$arg;
		if($mp3_file!~/ / && $mp3_file=~/(.*)__(.*)\.wav/)
		{
			my $artist=$1;
			my $track=$2;
			$artist=~s/_/ /g;
			$track=~s/_/ /g;
			
			$mp3_file=$artist.' - '.$track.'.mp3';
		}
		else
		{
			$mp3_file=~s/\.wav$/.mp3/;
		}
		my $mp3_file_esc=$mp3_file;
		# $mp3_file_esc=~s/([^A-Z0-9._-])/\\$1/gi;
		$mp3_file_esc=~s/(\")/\\\"/g;
		$mp3_file_esc=~s/\//\"\/\"/g;
		my $arg_esc=$arg;
		$arg_esc=~s/(\")/\\\"/g;
		$arg_esc=~s/\//\"\/\"/g;
		# $arg_esc=~s/([^A-Z0-9._-])/\\$1/gi;
		push(@cmds,"lame -h -V 2 --vbr-new \"$arg_esc\" \"$mp3_file_esc\" && rm \"$arg_esc\"");
		
		my $artist='';
		my $track_num='';
		my $title='';
		
		my $mp3_file_name=$mp3_file_esc;
		$mp3_file_name=~s/^.*?([^\/]+)/$1/;
		if($mp3_file_name=~/(.*) - ([0-9]+) (.*)\.mp3/)
		{
			$artist=$1;
			$track_num=$2;
			$title=$3;
		}
		elsif($mp3_file_name=~/(.*) - (.*)\.mp3/)
		{
			$artist=$1;
			$title=$2;
		}
		else
		{
			print "# file not recognized $mp3_file_name\n";
		}
		
		if(length($artist) > 0 && length($title) > 0)
		{
			# $artist=~s/([^A-Z0-9._-])/\\$1/gi;
			# $track_num=~s/([^A-Z0-9._-])/\\$1/gi;
			# $title=~s/([^A-Z0-9._-])/\\$1/gi;
			if(-f "/usr/bin/id3tag")
			{
				push(@cmds,"id3tag -a \"$artist\" -t \"$track_num\" -s \"$title\" \"$mp3_file_esc\"");
			}
			elsif(-f "/usr/bin/id3tool")
			{
				push(@cmds,"id3tool -r \"$artist\" --set-track=\"$track_num\" -t \"$title\" \"$mp3_file_esc\"");
			}
			elsif(-f "/usr/bin/id3ed")
			{
				push(@cmds,"id3ed -n \"$artist\" -k \"$track_num\" -s \"$title\" \"$mp3_file_esc\"");
			}
		}
	}
}

foreach my $cmd (@cmds)
{
	print $cmd."\n";
}
