#!/usr/bin/perl -w
require 'getopts.pl';
require '/usr/local/bin/whois.parse.func.pl';

sub usage
{
	print STDERR <<USAGE;
Usage: $script [OPTIONS] [site(s)]
Query whois for site(s) and parse the data returned.

       -h              Show this message
       -q              Be quiet
       -v <level>      Verbose output.
       -r <num>        Number of retrys before giving up. (Default: 5)
       site(s)         site(s) to query

USAGE
}

#just to remove warnings
undef($opt_h);
########################
Getopts("hv:qr:");
( $script = $0 ) =~ s,.*/,,;
usage, exit if $opt_h;
$opt_r = 5 if !$opt_r;
$opt_v = 0 if !$opt_v;
for ( $x = 0 ; $x < scalar(@ARGV) ; $x++ )
{
	print_whois( whois_query( $ARGV[$x], $opt_r ) );
	if ( $x + 1 < scalar(@ARGV) )
	{
		print "------------------------------------\n";
	}
}
