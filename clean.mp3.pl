#!/usr/bin/perl -w

# Clean mp3 filenames based on built-in rules of standardization.
# If it is called with no arguments it will search the current directory
# for any suspected mp3s.
#
# --Rules--
# - Primary goal: try to match song to 'artist - tracknum title (mix).mp3'
# - translate all underscores to spaces
# - capitalize the first letter of each word
# - the exception to the above rule is when the artis is in all caps but there
#   is some lower case in the filename assuming that that is the way that 
#   artist name is spelled
# - rename .pm3 files or .zip files that are really mp3s to .mp3
# - do nothing on a filename collision
# - remove a leading set of parentheses  ex. (alphaville) forever young.mp3
# - replace hex chars from url encoding to the matching char 
#   ex. alphaville%20-%20forever%20young.mp3
# - replace brackets or braces around mix title with parens
# - replace extensions like .mp3.1 or .mp3.2 or ... with simply .mp3 if there
#   is no collision or if the colliding file is smaller (based on gnapster 
#   download retrys)

# ---TODO---
# - document any rules missed
# - more documentation is needed to explain the line noise that make up the
#   regular expressions and misc rules below
# - check for .mp3.[0-9] in the directory scan
# - organize rules and allow them to be controlled with cmd line arguments
# - allow id3 based renaming if a switch is set.
# - add an interactive mode
# - allow verbose operation
# - allow demo operation where no files are actually modified
# - allow easy modifications of rule defaults
# - roll into packaging, generate a tarball, and submit to freshmeat

use strict;
use warnings;
use Getopt::Long;

sub usage
{
	print "
	usage: $0 [options]
	";
}

my %opts;
GetOptions('help'       =>\$opts{help},
           'verbose|v+' =>\$opts{verbose},
           'dryrun|n'   =>\$opts{dryrun},
) || usage();

if($opts{help})
{
	usage();
}

if(scalar(@ARGV)==0)
{
	my $cwd;
    chomp($cwd = `pwd`);
    opendir(DIR,$cwd);
    while(my $file=readdir(DIR))
    {
        if($file=~/\.([mp][pm][23]|zip)$/i)
        {
			push(@ARGV,$file);
		}
    }
}

foreach my $arg (@ARGV)
{
    print "in=$arg\n" if $opts{verbose};
    my $collide=0;
    my $in=$arg;
    my $arg1=$arg;
    $arg1=~s/(\W)/\\$1/g;
    my $type=`file $arg1`;
    if($type=~/MP3/i)
    {
		$arg=~s/\.zip$/.mp3/;
	}
    
    $arg=~s/\.pm([23])$/.mp$1/;
    if($arg!~/\.mp[23](\.[0-9]+)?$/i)
    {
		print "skipping $arg\n" if $opts{verbose} and $opts{verbose}>1;
        next;
    }
    
    if($arg eq uc($arg))
    {$arg=lc($arg);}
    
    $arg=~s/\.MP([23])$/.mp$1/;
    $arg=~s/\s+\.mp([23])$/.mp$1/;
    
    if($arg=~/^(.*\.mp[23])\.[0-9]+$/ && (!(-e $1) || (-s $in > -s $1)))
    {
        $arg=~s/\.[0-9]+$//;
        $collide=1;
        #print "arg=$arg\n";
    }
    
    $arg=~s/_([0-9a-f]{2})/%$1/ig;
    # print "arg=$arg\n";
    $arg=~s/_/ /g;
    
    $arg=~s/^\((.*)\)\s*-/$1 -/;
    
    if($arg!~/\(.*\)/ && $arg=~/[\[\{].*(mix|live).*[\]\}]/i)
    {
		$arg=~s/[\[\{](.*(mix|live).*)[\]\}]/($1)/i;
	}
    
    while($arg=~/%([0-9a-f]{2})/i)
    {
        my $hex=$1;
        my $chr=chr(hex($1));
        if($chr=~/\w|\s|[\(\)\[\]\{\}\'\"\!\&]/) # fix color highlighting "'
        {
            #print "translating [$hex] to {$chr}\n\n";
            $arg=~s/%$hex/$chr/g;
        }
        else
        {
            $arg=~s/%$hex/ $hex/g;
        }
    }
    if($arg!~/ - / &&  $arg=~/-/)
    {
        $arg=~s/-/ \- /g;
        if($arg=~/[01]?[0-9] - [0123]?[0-9] - (19|20)?[0-9]{2}/)
        {
            $arg=~s/([01]?[0-9]) - ([0123]?[0-9]) - ((19|20)?[0-9]{2})/$1-$2-$3/g;
        }
    }
    
    $arg=~s/\+/ /g;
    $arg=~s/ - ([0-9]{2}) - / - $1 /;
    
    while($arg=~/([a-z])([A-Z])([a-z])/)
    {
		$arg=~s/([a-z])([A-Z])([a-z])/$1 $2$3/g;
	}
	
	my $ext;
	if($arg=~/^(.*)\.(mp3)/)
	{
		$arg = $1;
		$ext = $2;
    }
	# print "gettinf ready to fix words $arg\n";
    my @words=split(/ /,$arg);

    for(my $x=0; $x<scalar(@words); $x++)
    {
        if(substr($words[$x],0,1) eq "(")
        {
            substr($words[$x],1,1)=uc(substr($words[$x],1,1));
        }
        elsif($words[$x] eq lc($words[$x]) or 
		      $words[$x] eq uc($words[$x]))
        {
            $words[$x]=ucfirst(lc($words[$x]));
        }
        # print "words[$x]=$words[$x]\n";
    }
    # $out=~s/(\W)/\\$1/g;
    
    my $out=join(" ",@words);
    $out=~s/\s+/ /g;
	
    $out .= '.' . $ext;
	
	print "$in --- $out\n" if $opts{verbose};

    # $in=~s/(\W)/\\$1/g;
    # $out=~s/(\W)/\\$1/g;
    if($in ne $out)
    {
        # $cmd="mv '$in' '$out'";
        # $cmd=~s/\\//g;
        #print $cmd."\n";
        if($collide==0 && -e $out)
        {
            print "Collision $out already exists\n";
        }
        else
        {
			if(!$opts{dryrun})
			{
				rename($in,$out);
			}
            print "rename($in,$out);\n";
        }
        #`mv $in $out`;
    }
}
