#!/bin/sh

if [ -z $1 ]
then
	ARGS="*"
else
	ARGS=$*
fi

if [ -f /usr/bin/du ];
then
	DU=/usr/bin/du
elif [ -f /bin/du ];
then
	DU=/bin/du
else
	echo "Unable to find 'du'"
	exit -1
fi

if [ -f /usr/bin/sort ];
then
	SORT=/usr/bin/sort
elif [ -f /bin/sort ];
then
	SORT=/bin/sort
else
	echo "Unable to find 'sort'"
	exit -1
fi

#test	
$DU -scb -- $ARGS | $SORT -n | human.pl
