#!/usr/bin/perl -w

# sort a list from stdin by length

@in=<>;

foreach $key (sort { length($b) <=> length($a) || $b cmp $a } @in)
  {print $key;}
