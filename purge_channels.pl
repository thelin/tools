#!/usr/bin/perl

use strict;
use warnings;
use DBI;
use Getopt::Long;


my $host = 'localhost';
my $database = "mythconverg";
my $user = "mythtv";
my $pass = "mythtv";
my $verbose;
my $dry_run;

sub usage
{
  print "
    usage: $0 [options]

    Where [options] is:
      --host, -h      - hostname or IP address of the mysql server
                        (default: \"$host\")
      --user, -u      - DBUSERNAME (default: \"$user\")
      --pass, -p      - DBPASSWORD (default: \"$pass\")
      --database, -d  - DATABASENAME (default: \"$database\")
      --verbose, -v   - Verbose (may be repeated)
      --dryrun, -n    - Dry Run
	";
	exit(0);
}

GetOptions('verbose+'=>\$verbose,
		'database|d=s'=>\$database,
		'host|h=s'=>\$host,
		'user|u=s'=>\$user,
		'pass|p=s'=>\$pass,
		'dryrun|n'=>\$dry_run,
		);

my $dbh = DBI->connect("dbi:mysql:database=$database:host=$host",
		"$user","$pass") or die "Cannot connect to database ($!)\n";

my $delete_sql = "DELETE FROM channel WHERE chanid = ?";
my $delete_sth = $dbh->prepare($delete_sql);

my @blank_channels;
{
	my $sql = qq|
	SELECT count(p.chanid) cnt,c.chanid, c.name
	FROM
	  channel c
	  LEFT JOIN program p ON c.chanid=p.chanid
	GROUP BY c.chanid
	HAVING cnt = 0
		|;

	my $sth = $dbh->prepare($sql);
	$sth->execute() or die "Could not execute ($sql)";
	
	
	while (my @row = $sth->fetchrow_array) {
		warn "deleting $row[1] $row[2]\n" if $verbose or $dry_run;
		if(not $dry_run)
		{
			$delete_sth->execute($row[1]) or die "Could not execute ($delete_sql with $row[1])";
		}
	}
}
