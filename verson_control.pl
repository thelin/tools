#!/usr/bin/perl
# build a program from its archive or source directory
$| = 1;

use strict;
use warnings;

my $debug;
my $dryrun;

my $default;

if(defined($ARGV[0]) && $ARGV[0]=~/^(cvs|svn|git)$/)
{
	$default=shift(@ARGV);
}

my $type;
if( -d '.svn')
{
	$type='svn';
}
elsif( -d 'CVS')
{
	$type='cvs';
}
else
{
	if( -d '.git')
	{
		$type='git';
	}
	else
	{
		`git status >/dev/null 2>/dev/null`;
		if($? == 0)
		{
			$type='git';
		}
	}
}


my $cmd;
if($type eq 'cvs')
{
	$cmd = 'cvs';
	if($default eq 'svn' and $ARGV[0] eq 'stat')
	{
		shift(@ARGV);
		unshift(@ARGV, 'up');
		unshift(@ARGV, '-n');
	}
	if($default eq 'svn' and $ARGV[0] eq 'info')
	{
		shift(@ARGV);
		unshift(@ARGV, 'stat');
	}
}
elsif($type eq 'svn')
{
	$cmd = 'svn';
}
elsif($type eq 'git')
{
	$cmd = 'git';
	if($ARGV[0] eq 'merge')
	{
		my $ff;
		for my $arg (@ARGV)
		{
			if($arg eq '--no-ff' or $arg eq '--ff')
			{
				$ff = 1;
				last;
			}
		}
		if(!$ff)
		{
			shift(@ARGV);
			unshift(@ARGV, 'merge');
			unshift(@ARGV, '--no-ff');
		}
	}
}
else
{
	$cmd=$default;
}

print $cmd." ".join(' ',@ARGV)."\n" if $debug;
if(!$dryrun)
{
	system($cmd,@ARGV);
}
