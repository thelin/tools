#!/usr/bin/perl

use strict;
use warnings;
use Getopt::Long;
use Term::ANSIColor;
use Data::Dumper;

my %options;
Getopt::Long::Configure ("gnu_getopt");
GetOptions(
    'help|h'          => \$options{help},
    'dry_run|n'       => \$options{dry_run},
    'verbose|v+'      => \$options{verbose},
) || help();

help() if $options{help};



#########################
#
print "Do Stuff here...\n";
run("echo shell stuff");
#
########################



sub help
{
    warn <<"END"
Usage: $0 [options] <files or directories>
    --help, -h                This message
    --dry_run, -n             Dry run.  Don't actually convert file(s)
    --verbose, -v             Verbose.  Show more output
END
;
    exit 1;
}

sub run
{
    my $cmd = shift;
    if($options{verbose} or $options{dry_run}) {
        print color("bold blue") . $cmd . color("reset") . "\n";
    }

    if(!$options{dry_run}) {
        # print "[[[$cmd]]]\n";
        system($cmd);
    }
    return $?;
}

