#!/bin/bash

if [ -z $1 ]
then
	echo "Usage: $0 [on|off|dns]"
	exit -1
fi

if [ `whoami` != 'root' ];
then
	sudo $0 $*
	exit;
fi

if [ $1 == 'on' ]
then
	if [ `/sbin/ifconfig ppp0 2>/dev/null |wc -l` -gt 1 ]
	then
		echo "link already up"
	else
		pon tun0
		sleep 15
		/sbin/route add -net 192.168.121.0 netmask 255.255.255.0 dev ppp0
		if [ `/sbin/ifconfig |grep 192.168.111|wc -l` == 0 ]
		then
			/sbin/route add -net 192.168.101.0 netmask 255.255.255.0 dev ppp0
			/sbin/route add -net 192.168.111.0 netmask 255.255.255.0 dev ppp0
			/sbin/route add -net 192.168.2.0 netmask 255.255.254.0 dev ppp0
		fi
		sleep 10
		echo vpn on
	fi
elif [ $1 == 'off' ]
then
	poff tun0 >/dev/null
	if [ `/sbin/route -n |grep 192.168.101.*ppp0|wc -l` == 1 ]
	then
		/sbin/route del -net 192.168.101.0 netmask 255.255.255.0 dev ppp0
	fi
	if [ `/sbin/route -n |grep 192.168.111.*ppp0|wc -l` == 1 ]
	then
		/sbin/route del -net 192.168.111.0 netmask 255.255.255.0 dev ppp0
	fi
	if [ `/sbin/route -n |grep 192.168.121.*ppp0|wc -l` == 1 ]
	then
		/sbin/route del -net 192.168.121.0 netmask 255.255.255.0 dev ppp0
	fi
	if [ `/sbin/route -n |grep 192.168.2.*ppp0|wc -l` == 1 ]
	then
		/sbin/route del -net 192.168.2.0 netmask 255.255.254.0 dev ppp0
	fi
	echo vpn off
fi

RESOLV=/etc/resolv.conf.no-vpn

route -n|grep 192.168.101.0 >/dev/null
if [ $? -eq 0 ]
then
	ping -c 1 192.168.101.1 >/dev/null
	if [ $? -eq 0 ]
	then
		echo "VPN is up"
		RESOLV=/etc/resolv.conf.ili-vpn
	else
		echo "VPN is down"
	fi
else
	echo "VPN is down1"
fi

if [ -L /etc/resolv.conf -o \
	 -f /etc/resolv.conf.no-vpn -a -f /etc/resolv.conf -a \
	 `cat /etc/resolv.conf|md5sum|cut -f1 -d' '` == `cat /etc/resolv.conf.no-vpn|md5sum|cut -f1 -d' '` ]
then
	rm /etc/resolv.conf
elif  [ -f /etc/resolv.conf ]
then
	mv --backup=numbered /etc/resolv.conf /etc/resolv.conf.no-vpn
fi
ln -s $RESOLV /etc/resolv.conf
