#!/usr/bin/perl 

use warnings;
use strict;
use DBI;

my $ifconfig=`/sbin/ifconfig`;

my @ifconfig=split /\n\n/,$ifconfig;
# use Data::Dumper; print Dumper(@ifconfig); # EKT

my @interfaces;

for my $interface_line (@ifconfig)
{
	if($interface_line =~ /^([a-z]+[0-9]*(?:\:[0-9]+)?)\s.*inet addr:([0-9.]+)/s)
	{
		# print "--- $1 ($2)\n";
		if($1 ne 'lo')
		{
			push(@interfaces,{'iface'=>$1,'ip'=>$2});
		}
	}
}

my $dbh = DBI->connect("DBI:mysql:database=pdns:host=proteus.generation-i.com", 'pdns_read', 'ReedM3');

for(my $x=0; $x<scalar(@interfaces); $x++)
{
	my $iface_info = $interfaces[$x];
	my $query = "select name from records where content=".$dbh->quote($iface_info->{'ip'});
	# print "query=$query\n" if defined($opt_v) && $opt_v>2;
	my $sth = $dbh->prepare($query) || die "Error:" . $dbh->errstr . "\n";
	$sth->execute || die "Error:" . $dbh->errstr . "\n";
	
	my @names;
	while(my ($name)=$sth->fetchrow_array)
	{
		# use Data::Dumper; print Dumper($name); # EKT
		push(@names,$name);
	}
	$interfaces[$x]->{names}=join(', ', @names);
}

# use Data::Dumper; print Dumper(@interfaces); # EKT

for my $iface (@interfaces)
{
	printf("%-10s\t%s\n", $iface->{iface}, $iface->{ip});
	printf("\t\t%s\n\n\n", $iface->{names});
}
