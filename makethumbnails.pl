#!/usr/bin/perl -w
# make thumbnails of a given size of all images in a directory or a file
# thumbnails will be named as the image name with a preceding .
# for example: car.gif becomes .car.gif

use strict;
use warnings;
use Data::Dumper;
use Getopt::Long;
use File::Basename;


my %options;
Getopt::Long::Configure ("gnu_getopt");
GetOptions(
	'help|h'      => \$options{help},
	'width|x=i'   => \$options{width},
	'height|y=i'  => \$options{height},
	'force|f'     => \$options{force},
	'recurse|r'   => \$options{recursive},
	'colors|c=i'  => \$options{colors},
	'quality|q=i' => \$options{quality},
	'prefix|p=s'  => \$options{prefix},
	'suffix|s=s'  => \$options{suffix},
	'type|t=s'    => \$options{type},
	'dry_run|n'   => \$options{dry_run},
	'verbose|v+'  => \$options{verbose},
	'dir|D=s'     => \$options{dir},
	'audiobook|a' => \$options{audiobook},
) || help();

help() if $options{help};

if(scalar(@ARGV) == 0)
{
	push(@ARGV, '.');
}

sub help
{
	print STDERR <<USAGE;

Usage: $0 [OPTIONS] [file(s)] [directory(s)]
Make thumbnails of a given size of all images in a directory or a file.
Thumbnails will be named as the image name with a preceding .
For example: car.gif becomes .car.gif

    --width, -x <width> 	width of the thumnail   (default: 100)
    --height, -y <height>	height of the thumnail  (default: 100)
    --force, -f         	force to the given size (default: only use the larger size)
    --recurse, -r       	make thumnails in all subdirectories recursively
    --color, -c <1-255> 	Max colors in GIFs
    --quality, -q <1-100>	JPG quality
    --prefix, -p <prefix>	output prefix for each file (default: .)
    --suffix, -s <suffix>	output suffix for each file (conflicts with --prefix)
    --dir, -D <dir>     	thumbnail directory
    --type, -t <type>   	Force a given file type for output
    --dry_run, -n        	dryrun
    --help, -h          	help (this message)
    --verbose, -v       	verbose (may be repeated for more verbosity)
   file(s)	image(s) for to create a thumbnail of
   directory(s)	directory(s) of images to create a thumbnail of
USAGE
}

if(!$options{width} and !$options{height})
{
	$options{width} ||= 100;
	$options{height} ||= 100;
}

$options{width} ||= '';
$options{height} ||= '';

if(!$options{prefix} and !$options{suffix} and !$options{dir})
{
	$options{prefix} = '.';
}
undef($options{width})  if $options{width} == -1;
undef($options{height}) if $options{height} == -1;

warn '@ARGV: '.Dumper(\@ARGV) if defined $options{verbose};


for my $arg (@ARGV)
{
	if ( -d $arg && $options{recursive} )
	{		
		# print "directory $arg\n";
		opendir( DIR, $arg );
		while ( my $file = readdir(DIR) )
		{
			if ( substr( $file, 0, 1 ) ne "." )
			{
				push( @ARGV, $arg . "/" . $file );
			}
		}
		closedir(DIR);
		warn '~~~'. Dumper(\@ARGV) if $options{verbose};
		next;
	}

	$arg=~s/^\.\///;
	if ( -f $arg and $arg !~ /(^|\/)\.[^\/]*$/ )
	{
		my $thumb = $arg;
		my $dir = $options{dir} || '.';
		if ( $thumb =~ /\// )
		{
			$thumb =~ s/\/([^\/]*)$/\/$options{prefix}$1/;
			my $dir = $thumb;
			$dir =~ s:/[^\/]*$:/:;
		}
		
		if ( !( -d $dir ) )
		{
			if($options{verbose})
			{
				print "making directory: $dir\n";
			}
			if(!$options{dry_run})
			{
				mkdir( $dir, 0644 ) || die "Error $! ($dir)";
			}
		}
		
		if($options{prefix})
		{
			$thumb = $options{prefix} . $thumb;
		}
		elsif($options{suffix})
		{
			$options{suffix}=~s/^([a-z0-9])/.$1/;
			if(my @out = $thumb=~/(.*)(\..{2,4})/)
			{
				$thumb = $out[0] . $options{suffix} . $out[1];
			}
			else
			{
				$thumb .= $options{sufffix};
			}
			
		}
		elsif(!$dir or $dir eq '.')
		{
			$thumb = '.' . $thumb;
		}
		
		if ( defined($options{type}) )
		{
			if ( substr( $options{type}, 0, 1 ) ne "." )
			{
				$options{type} = "." . $options{type};
			}
			if ( $thumb =~ /^(.*)(\.gif|\.jpg|\.pnm)$/i )
			{
				$thumb = $1 . $options{type};
			}
			else
			{
				$thumb = $thumb . $options{type};
			}
		}
		if ( $arg =~ /\.(gif|jpg|pnm)$/i )
		{
			my @cmd_args = ();
			if ( $arg =~ /\.gif/i && $thumb =~ /\.gif/i )
			{
				my $color = `giftool -p '$arg'|grep "[0-9]:"|tail -n 1`;
				chop($color);
				$color =~ s/([0-9]*):.*$/$1/;
				if ( defined($options{colors}) && $color > $options{colors} )
				{
					$color = $options{colors};
				}
				push(@cmd_args, "-colors $color");
			}
			if ( defined($options{quality}) && $thumb =~ /\.jpg/i )
			{
				push(@cmd_args, "-quality " . $options{quality})
			}
			if ( defined($options{width}) && defined($options{height}) )
			{
				push(@cmd_args, "-geometry " . $options{width} . 'x' . $options{height} . 
					($options{force}? '!': ''));
			}
			push(@cmd_args, cmd_escape($arg));
			push(@cmd_args, cmd_escape($dir.'/'.$thumb));
			my $cmd = "convert " . join(' ', @cmd_args);
			run($cmd);
		}
		print "thumnail $thumb created\n";
	}
}

sub cmd_escape
{
	my $str = shift;
	$str=~s/([^A-Z0-9.\/-])/\\$1/gi;
	return $str;
}

sub run
{
	my $cmd = shift;
	if($options{verbose} or $options{dry_run})
	{
		print "\n" . color("bold blue") . $cmd . color("reset") . "\n";
	}
	
	if(!$options{dry_run})
	{
		system($cmd);
	}
	return $?;
}
