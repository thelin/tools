#!/bin/bash

if [ `whoami` != 'root' ];
then
	sudo $0
	exit;
fi

#iwconfig eth1 essid Typhoon channel 3
#iwconfig eth1 essid the_land channel 6

uname -r
#wpa_driver="ipw"  # if < 2.6.13
wpa_driver="wext" # if >= 2.6.13

killall wpa_supplicant
killall dhclient3

wpa_supplicant -iath0 -c /etc/wpa_supplicant_a.conf -D madwifi &
wpa_supplicant -ieth1 -c /etc/wpa_supplicant.conf -D $wpa_driver &
#wpa_supplicant -ieth1 -c /etc/wpa_supplicant.conf -D $wpa_driver -d &

dhclient3 -pf /var/run/dhclient.ath0.pid -lf /var/lib/dhcp3/dhclient.ath0.leases  ath0
dhclient3 -pf /var/run/dhclient.eth1.pid -lf /var/lib/dhcp3/dhclient.eth1.leases  eth1

rm -f /home/*/.ssh/master-*@*
