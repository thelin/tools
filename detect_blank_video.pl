#!/usr/bin/perl

use strict;
use warnings;
use Data::Dumper;
use Getopt::Long;
use MythTV;
use constant EMAIL => 'eric.thelin@gmail.com';


my $help;
my $initial_offset = 155;
my $verbose = 0;
my $skip = 72;
my $tmp_dir = "/tmp/detect_$$";
my $age;
my $delete;
my @ext;
my @default_ext = ('mpg','nuv');

Getopt::Long::Configure ("gnu_getopt");
GetOptions(
	'help|h'     => \$help,
	'offset|o=i' => \$initial_offset,
	'skip|s=s'   => \$skip,
	'tmp_dir|t'  => \$tmp_dir,
	'delete|d'   => \$delete,
    'age|a=i'  => \$age,
    'ext|e=s'    => \@ext,
	'verbose|v+' => \$verbose,
) || help();

help() if $help;

if(!scalar(@ext))
{
    @ext = @default_ext;
}
@ext = split(/[|,]/,join(',',@ext));
if(scalar(@ARGV) == 0)
{
	push(@ARGV, '.');
}

my $Myth = new MythTV();


mkdir($tmp_dir, 0700);
my @blanks;

FILE:
for my $file (@ARGV)
{
    if(-d $file)
    {
        opendir(DIR, $file);
        while(my $f = readdir(DIR))
        {
            if($f=~/^\./)
            {
                next;
            }
            push(@ARGV, $file.'/'.$f);
        }
        close(DIR);
        next;
    }
    if($age)
    {
        my $mtime = (stat($file))[9];
        #print "$file - $mtime\n";
        if(time() - $mtime> $age * 60 * 60)
        {
            warn 'skipping '.$file." due to age\n" if $verbose > 1;
            next;
        }
    }
    if(scalar(@ext))
    {
        my $found = 0;
        for my $ext (@ext)
        {
            if($file=~/\.$ext$/ or $ext eq '*')
            {
                $found = 1;
                last;
            }
        }
        if(!$found)
        {
            warn 'skipping '.$file." because it does not contain a valid extension\n" if $verbose > 1;
            next;
        }
    }
    warn 'looking at '.$file."\n" if $verbose;

    my $file_dir = $file;
    $file_dir =~ s#.*/##;
    $file_dir =~s/([^a-z0-9._-])/\\$1/gi;
    my $dir = $tmp_dir."/$file_dir";
    mkdir($dir, 0700);
    for(my $offset = $initial_offset; $offset < ($initial_offset + ($skip*10)); $offset+=$skip)
    {
        my $mplayer_out = `mplayer $file -ss $offset -nosound -vo jpeg:outdir=$dir/$offset -frames 1 >/dev/null 2>&1`;
        warn '$mplayer_out='.Dumper(\$mplayer_out) if $verbose > 2;
        rename("$dir/$offset/00000001.jpg", "$dir/${offset}-00000001.jpg");
        rmdir("$dir/$offset");
    }
    opendir(DIR, $dir);
    my $size;
    while(my $f = readdir(DIR))
    {
        if($f!~/jpg$/)
        {
            next;
        }
        if(!defined($size))
        {
            $size = -s "$dir/$f";
            warn '$size='.Dumper(\$size) if $verbose > 1;
        }
        else
        {
            my $new_size = -s "$dir/$f";
            warn '$new_size='.Dumper(\$new_size) if $verbose > 2;
            if($size != $new_size)
            {
                print "different is GOOD $file\n" if $verbose;
                `rm -rf $dir`;
                close(DIR);
                next FILE;
            }
        }
    }
    close(DIR);
    `rm -rf $dir` if !$verbose;
    print "$file is BLANK\n";
    push(@blanks, $file);
    if($delete)
    {
        my $file_obj = $Myth->new_recording($file);
        if($file_obj)
        {
            # use Data::Dumper; warn Dumper($file_obj); # EKT
            my $file_obj_str = $file_obj->to_string();
            my $out = $Myth->backend_command(['DELETE_RECORDING', $file_obj_str], '0');
            $out = $Myth->backend_command(['FORGET_RECORDING', $file_obj_str], '0');
            $out = $Myth->backend_command(['FORCE_DELETE_RECORDING', $file_obj_str], '0');
            print "deleted and forgot $file\n";
        }
        else
        {
            print "could not delete $file because it was not found in the database\n";
        }
    }

}
unlink($tmp_dir) if !$verbose;
send_notification(join("\n",@blanks),"MythTV is making blank recordings");

sub help
{
	warn <<"END"
Usage: $0 [options] <files or directories>
  --help, -h                 This message
  --offset <sec>, -o <sec>   How many seconds to skip before the first thumbnail
  --skip <sec>, -s <sec>     How many seconds to skip between thumbnails
  --tmp_dir <dir>, -t <dir>  Temporary directory
  --delete, -d               Automatically delete file
  --age <hours>, -a <hours>  Max age of the file to consider in hours
  --ext=<ext>, -e <ext>      Valid extensions (default: mpg, nuv)
  --verbose, -v              Verbose.  Show more output (can be repeated for
                             even more output)
END
;
	exit 1;
}


sub send_notification
{
    my $message     = shift;
    my $subject     = shift;
    my $destination = shift || EMAIL;

    if($ENV{PWD})
    {
        print(localtime() . " $subject:\t" . $destination."\n");
    }
    open(MAIL,'|mail -s "'.$subject.'" ' . $destination);
    print MAIL $message;
    close(MAIL);

}
