#!/usr/bin/perl

if( $ARGV[0] == 'port' )
{
	my $old_process=`ps aux|grep '5910:localhost:5900'|grep -v grep|cut -c10-15`;
	
	if(length($old_process) > 0)
	{
		print `kill $old_process`;
	}
	print `ssh -L 5910:localhost:5900 jupiter -N &`;
	sleep 5;
	print `vncviewer localhost:10`;
}
else
{
	print `ssh -XC jupiter vncviewer -truecolour localhost:0`;
}
