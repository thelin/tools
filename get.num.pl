#!/usr/bin/perl


$url=$ARGV[0];
foreach $url (@ARGV)
{
	undef($begin);
	undef($end);
	undef($pad_size);
# print "orig=$url\n";

	if(!($url=~/\{.*\}/) && $url=~/(([0-9]+)(\.[a-z]+$))/i)
	{
		$whole=$1;
		$num=$2;
		$ext=$3;
		$pad=length($num);
		$num=~s/^0//g;
		$url=~s/$whole/{$pad,$num}$ext/;
		# print "orig=$url\n";
	}
	if($url=~/\{(.*)\}/)
	{
		$template=$1;
		# print "replace=$template\t from url=$url\n";
		if($template=~/^([0-9]+),/)
		{
			$pad_size=$1;
		}
		else
		{
			$pad_size=0;
		}
		if($template=~/([0-9]+)-([0-9]*)$/)
		{
			$begin=$1;
			$end=$2;
		}
		elsif($template=~/([0-9]+)$/)
		{
			$begin=$1;
			$end='';
		}
		elsif(length($template)==0)
		{
			$begin=1;
		}
		# print "pad_size=$pad_size\n";
		# print "begin=$begin\n";
		# print "emd=$end\n";
		# exit;
		for($x=$begin; $x<$end || !defined($end) || length($end)==0; $x++)
		{
			$num=$x;
			while(length($num)<$pad_size)
			{
				$num="0$num";
			}
			($new=$url)=~s/\{$template\}/$num/;
			# print "new=$new (x=$x\tnum=$num)\n";
			$new=~s/"/\\"/g;
			$out=`wget  "$new" 2>&1`;
			if($out!~/$ext(\.[0-9]*)?\' saved/)
			{
				print "[$ext] out=$out\n";
				# print "no end\n";
				$end=0;
			}
	
			if((!defined($end) || length($end)==0) && $?!=0)
			{
				# print "no end\n";
				$end=0;
			}
		}
	}
}
