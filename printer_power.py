#!/usr/bin/python

import RPi.GPIO as GPIO
import sys

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

# set pin to negative to invert it (so that off becomes on and on becomes off
# this makes sense if you want the default state to be on
deviceList = { "printer": 4, "light": 2, "light1": 3 }

def help(exitcode):
    print "Usage: " + sys.argv[0] + " <device[,device2,device3,...]> <on|off|toggle>";
    print "    Available devices are: " + ", ".join(deviceList.keys())
    print
    sys.exit(exitcode);


if len(sys.argv) != 3:
    help(-1)


devices = sys.argv[1].split(",")

for device in devices:
    if (not (device in deviceList.keys())):
        print "Invalid Device: " + sys.argv[0]
        help(-2)

    pin = deviceList[device]

    if sys.argv[2] == "on":
        action = 1
    elif sys.argv[2] == "off":
        action = 0
    elif sys.argv[2] == "toggle":
        action = -1
    else:
        print "Invalid action: " + sys.argv[2]
        help(-3)


    on = GPIO.HIGH
    off = GPIO.LOW
    if pin < 0:
        if (action == 0 or action == 1):
            on = GPIO.LOW
            off = GPIO.HIGH
            pin = pin * -1

    GPIO.setup(pin, GPIO.OUT)

    if action == 1:
        GPIO.output(pin, on)
    elif action == 0:
        GPIO.output(pin, off)
    elif action == -1:
        state = GPIO.input(pin)
        if (state == on):
            GPIO.output(pin, off)
        else:
            GPIO.output(pin, on)
