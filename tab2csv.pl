#!/usr/bin/perl

use strict;
use warnings;

use Text::CSV_XS;

my $in_file;
my $out_file;

if(scalar(@ARGV) == 0)
{
	$in_file = '-';
	$out_file = '-';
}
elsif(scalar(@ARGV) == 1)
{
	if(!-f $ARGV[0] or !-r $ARGV[0])
	{
		die "Error: Tab file '$ARGV[0]' is not readable.\n";
	}
	$in_file = $out_file = $ARGV[0];
	if($in_file=~/\.tab$/)
	{
		$out_file =~ s/tab$/csv/;
	}
	
	if(!$out_file or -f $out_file)
	{
		if($out_file !~ /\.csv$/)
		{
			$out_file = $out_file . '.csv';
		}
		while(-f $out_file)
		{
			if($out_file=~/\.([0-9]+).csv/)
			{
				my $new_num = $1 + 1;
				$out_file=~s/\.$1\.csv$/\.$new_num.csv/;
			}
			else
			{
				$out_file=~s/\.csv$/\.1.csv/;
			}
		}
	}
}
elsif(scalar(@ARGV) == 2)
{
	if(!-f $ARGV[0] or !-r $ARGV[0])
	{
		die "Error: Tab file '$ARGV[0]' is not readable.\n";
	}
	if(-f $ARGV[1])
	{
		die "Error: CSV file '$ARGV[1]' already exists.\n";
	}
	$in_file  = $ARGV[0];
	$out_file = $ARGV[1];
}
else
{
	die "Usage: $0 <in_file.tab> [<out_file.csv]\n";
}


my $csv = Text::CSV_XS->new ({
								 binary   => 1,
								 sep_char => ",",
								 eol      => "\n",
							 }) or  die "Cannot use CSV: ".Text::CSV->error_diag ();
my $in_fh;
if($in_file eq '-')
{
	no strict 'subs';
	$in_fh = STDIN;
}
else
{
	open $in_fh, "<:encoding(utf8)", $in_file or die "$0: Cannot open '$in_file' $!";
}

my $out_fh;
if($out_file eq '-')
{
	no strict 'subs';
	$out_fh = STDOUT;
}
else
{
	open $out_fh, ">:encoding(utf8)", $out_file or die "$0: Cannot open '$out_file': $!";
}
while (my $line = <$in_fh>)
{
	chomp($line);
	my @row = split /\t/, $line;
	$csv->print($out_fh, \@row);
	
}
$csv->eof or $csv->error_diag ();
close $in_fh;

close $out_fh or die "#0: Cannot close $out_file' $!";
