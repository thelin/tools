#!/usr/bin/php
<?php

$outDir = "/media/virgo_data/TV/";
$inDir = "/media/download/torrent_download/";
$dryRun = false;
$verbose = true;
$quiet = true;

$noiseLevel = 0;
if ($verbose) {
    $noiseLevel++;
}
if ($quiet) {
    $noiseLevel = 0;
}

$shows = array();
$showsDir = getDir($outDir);
foreach ($showsDir as $file) {
    if (substr($file, 0, 1) != '.') {
        $shows[getKey($file)] = $file;
    }
}

$epDir = getDir($inDir);
foreach ($epDir as $epFile) {
    $epKey = getKey($epFile);
    foreach ($shows as $showKey => $showDir) {
        if (strstr($epKey, $showKey)) {
            out("$showDir ~~~~ $epFile\n");
            if (is_dir($inDir . $epFile)) {
                $epFileDir = getDir($inDir . $epFile);
            } else {
                $epFileDir[] = $epFile;
                $epFile = '.';
            }
            foreach ($epFileDir as $f) {
                $fileWithoutYear = preg_replace('/(19|20)\d\d/', '', $f);

                if (preg_match('/\.(mkv|mp4|avi|mpg|m4v|wmv|srt)$/', $f, $out1)) {
                    $extension = $out1[1];
                    if (preg_match('/S0?(\d{1,2})E0?(\d{1,2})/i', $f, $out2)) {
                        $seasonNum = $out2[1];
                        $episodeNum = $out2[2];
                    } elseif (preg_match('/[\. ](\d{1,2}?)(\d{1,2})[\. ]/i', $fileWithoutYear, $out2)) {
                        $seasonNum = $out2[1];
                        $episodeNum = $out2[2];
                    }
                    if (!empty($seasonNum) && !empty($episodeNum)) {
                        
                        if (file_exists("$outDir$showDir/Season $seasonNum")) {
                            $seasonDir = "$outDir$showDir/Season $seasonNum";
                        } elseif (file_exists("$outDir$showDir/Season 0$seasonNum")) {
                            $seasonDir = "$outDir$showDir/Season 0$seasonNum";
                        } elseif (file_exists("$outDir$showDir/Season 01") && strlen($seasonNum) == 1) {
                            $seasonDir = "$outDir$showDir/Season 0$seasonNum";
                            if (!$dryRun) {
                                mkdir($seasonDir, 0755);
                            }
                        } else {
                            $seasonDir = "$outDir$showDir/Season $seasonNum";
                            if (!$dryRun) {
                                mkdir($seasonDir, 0755);
                            }
                        }
                        out("Season {$seasonNum} Episode {$episodeNum}  -- $epFile/$f\n");
                        $showName = preg_replace('/ \([0-9]{4}\)/', '', $showDir);

                        $newEpFile = sprintf('%s - S%02dE%02d.%s',$showName, $seasonNum, $episodeNum, $extension);
                        $fromFile = "$inDir$epFile/$f";
                        $toFile = "$seasonDir/$newEpFile";
                        if (file_exists($toFile)) {
                            out("ERROR: $toFile already exists\n", -1);
                        } else {
                            out("mv \"$fromFile\" \"$toFile\"\n");
                            if (!$dryRun) {
                                rename($fromFile, $toFile);
                            }
                        }
                    }
                } elseif (preg_match('/sample/i', $f) && is_dir("$inDir$epFile/$f")) {
                    $sampleDir = "$inDir$epFile/$f";
                    $sampleFiles = getDir($sampleDir);
                    out(print_r($sampleFiles, true), 1);
                    foreach ($sampleFiles as $sampleFile) {
                        out("unlink $sampleDir/$sampleFile\n");
                        if (!$dryRun) {
                            unlink($sampleDir.'/'.$sampleFile);
                        }
                    }
                    rmdir($sampleDir);
                } elseif (preg_match('/(torrent.downloaded.from.*txt|\.com|\.txt$|\.nfo$|sample)/i', $f)) {
                    out("unlink $inDir$epFile/$f\n");
                    if (!$dryRun) {
                        unlink("$inDir$epFile/$f");
                    }
                }
            }
            out(print_r($epFileDir, true), 1);
        }
    }
    $remainingFiles = getDir("$inDir$epFile");
    if (is_dir($inDir.$epFile) && count($remainingFiles) == 0) {
        out("rmdir $inDir$epFile\n");
        if (!$dryRun) {
            rmdir("$inDir$epFile");
        }
    }
}

function getKey($name)
{
    $key = preg_replace('/[^A-Za-z0-9]/', '', $name);
    if (preg_match('/(19|20)\d\d/', $key)) {
        $key = preg_replace('/(19|20)\d\d/', '', $key);
    }
    return strtolower($key);
}

function getDir($dir)
{
    $files = array();
    if (is_dir($dir)) {
        if ($dh = opendir($dir)) {
            while (($file = readdir($dh)) !== false) {
                if (substr($file, 0, 1) == '.') {
                    continue;
                }
                $files[] = $file;
            }
            closedir($dh);
        }
    }
    return $files;
}

function out($str, $level=0)
{
    global $noiseLevel;
    if ($level > $noiseLevel) {
        print $str;
    }
}
