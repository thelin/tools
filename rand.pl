#!/usr/bin/perl -w

# sort a list from standard in randomly
srand();

if(scalar(@ARGV)==0)
  {@in=<STDIN>;}
else
  {@in=@ARGV;}
  
while($_=shift @in)
{
  push(@list,rand(10000)."~~".$_);
}
@out=sort (@list);

foreach $line (@out)
{
  $line=~s/^[0-9.]*~~//;
  print $line;
  if(scalar(@ARGV)>0)
    {print " ";}
}
